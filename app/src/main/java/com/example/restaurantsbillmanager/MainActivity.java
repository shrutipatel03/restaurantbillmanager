package com.example.restaurantsbillmanager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.TextView;

//import com.example.restaurantsbillmanager.activity.HomeActivity;
import com.example.restaurantsbillmanager.activity.LoginActivity;
//import com.example.restaurantsbillmanager.activity.RestartActivity;
import com.example.restaurantsbillmanager.database.MySharedPreference;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    NavigationView navigationView;
    MySharedPreference mySharedPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.hideOverflowMenu();

        mySharedPreference = new MySharedPreference(MainActivity.this);
        navigationView = findViewById(R.id.nav_view);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        Menu menu = navigationView.getMenu();
        MenuItem logout = menu.findItem(R.id.nav_logOut);
       /* MenuItem startOrder = menu.findItem(R.id.nav_startOrder);
        startOrder.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intent = new Intent(MainActivity.this, RestartActivity.class);
                startActivity(intent);
                return true;
            }
        });*/
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_table,R.id.nav_item,R.id.nav_startOrder,R.id.nav_orderList,R.id.nav_viewRunningOrderList,R.id.nav_tableWiseReport,R.id.nav_logOut)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        logout.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                mySharedPreference.logout();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;

            }
        });
        initAllControls();
    }

    private void initAllControls() {
        View view = navigationView.getHeaderView(0);
        TextView txtIcon = (TextView)view.findViewById(R.id.tvIcon) ;
        TextView txtRestaurantName = (TextView) view.findViewById(R.id.tvRestuarantName);
        txtRestaurantName.setText(mySharedPreference.getEmail());
        Log.d("MainActivity","R name"+txtRestaurantName);
        if (mySharedPreference.getRestourantName().trim().length()>0) {
            //txtIcon.setText(mySharedPreference.getRestourantName().substring(0, 1).toUpperCase());
            txtIcon.setText(mySharedPreference.getRestourantName());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}
