package com.example.restaurantsbillmanager.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.restaurantsbillmanager.database.OrderItemTable;
import com.example.restaurantsbillmanager.database.OrderTable;

import java.util.List;

public class OrderListModel implements Parcelable {
    public OrderTable orderTable;
    List<OrderItemTable> orderItemTableList;

    public OrderListModel()
    {

    }

    protected OrderListModel(Parcel in) {
        orderTable = in.readParcelable(OrderTable.class.getClassLoader());
        orderItemTableList = in.createTypedArrayList(OrderItemTable.CREATOR);
    }

    public static final Creator<OrderListModel> CREATOR = new Creator<OrderListModel>() {
        @Override
        public OrderListModel createFromParcel(Parcel in) {
            return new OrderListModel(in);
        }

        @Override
        public OrderListModel[] newArray(int size) {
            return new OrderListModel[size];
        }
    };

    public OrderTable getOrderTable() {
        return orderTable;
    }

    public void setOrderTable(OrderTable orderTable) {
        this.orderTable = orderTable;
    }

    public List<OrderItemTable> getOrderItemTableList() {
        return orderItemTableList;
    }

    public void setOrderItemTableList(List<OrderItemTable> orderItemTableList) {
        this.orderItemTableList = orderItemTableList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(orderTable, flags);
        dest.writeTypedList(orderItemTableList);
    }
}
