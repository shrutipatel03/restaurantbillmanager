package com.example.restaurantsbillmanager.model;

import com.example.restaurantsbillmanager.database.ItemTable;
import com.example.restaurantsbillmanager.database.TableTable;

import java.util.List;

public class SaveOrderModel {

    String customer_name;
    String customer_mobile;
    String table_number;
    String date;
//    List<TableName> tableTableList;
    List<OrderItem> orderItemList;
    double order_amount;

    public SaveOrderModel(String customer_name, String customer_mobile,String table_number, String date, List<OrderItem> orderItemList, double order_amount) {
        this.customer_name = customer_name;
        this.customer_mobile = customer_mobile;
        this.table_number = table_number;
//        this.tableTableList = tableTableList;
        this.date = date;
        this.orderItemList = orderItemList;
        this.order_amount = order_amount;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_mobile() {
        return customer_mobile;
    }

    public void setCustomer_mobile(String customer_mobile) {
        this.customer_mobile = customer_mobile;
    }

    public String getTable_number() {
        return table_number;
    }

    public void setTable_number(String table_number) {
        this.table_number = table_number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public double getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(double order_amount) {
        this.order_amount = order_amount;
    }

    public static class OrderItem{

        ItemTable itemTable;
        double qty;
        double total_amount;

        public OrderItem(ItemTable itemTable, double qty, double total_amount) {
            this.itemTable = itemTable;
            this.qty = qty;
            this.total_amount = total_amount;
        }

        public ItemTable getItemTable() {
            return itemTable;
        }

        public void setItemTable(ItemTable itemTable) {
            this.itemTable = itemTable;
        }

        public double getQty() {
            return qty;
        }

        public void setQty(double qty) {
            this.qty = qty;
        }

        public double getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(double total_amount) {
            this.total_amount = total_amount;
        }
    }

//    public static class TableName{
//        TableTable tableTable;
//
//        public TableName(TableTable tableTable) {
//            this.tableTable = tableTable;
//        }
//
//        public TableTable getTableTable() {
//            return tableTable;
//        }
//
//        public void setTableTable(TableTable tableTable) {
//            this.tableTable = tableTable;
//        }
//    }
}

