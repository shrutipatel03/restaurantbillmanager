package com.example.restaurantsbillmanager.myUtils;

public interface DeleteConfirmDialogListener {
    public void onDialogOkButtonClicked();
    public void onDialogCancelButtonClicked();

}
