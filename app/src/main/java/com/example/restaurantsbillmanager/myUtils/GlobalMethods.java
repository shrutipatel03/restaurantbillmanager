package com.example.restaurantsbillmanager.myUtils;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.restaurantsbillmanager.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GlobalMethods {

    public static String mobilePattern = "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$";

    public static boolean validPersonName(String txt) {
        String regx = "^[a-zA-Z\\s]+$";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }

    public static boolean isValidEmail(String target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    public static void showDeleteConfirmDialog(Context context, DeleteConfirmDialogListener deleteConfirmDialogListener) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_confirm_dialog);
        Button btnSplashUpdate = (Button) dialog.findViewById(R.id.btnSplashUpdate);
        Button btnSplashCancel = (Button) dialog.findViewById(R.id.btnSplashCancel);
        // if button is clicked, close the custom dialog
        btnSplashCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteConfirmDialogListener.onDialogCancelButtonClicked();
                //Toast.makeText(getApplicationContext(), ".!!", Toast.LENGTH_SHORT).show();
            }
        });

        btnSplashUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                deleteConfirmDialogListener.onDialogOkButtonClicked();

            }
        });
        dialog.show();
    }
}
