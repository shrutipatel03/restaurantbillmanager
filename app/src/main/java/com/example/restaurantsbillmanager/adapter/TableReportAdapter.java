package com.example.restaurantsbillmanager.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.database.OrderItemTable;
import com.example.restaurantsbillmanager.model.OrderListModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class TableReportAdapter extends RecyclerView.Adapter<TableReportAdapter.MyViewHolder> {
    List<OrderListModel> orderListModelList;
    Activity activity;
    public TableReportAdapter(Activity activity,List<OrderListModel> orderListModelList) {
        this.activity = activity;
        this.orderListModelList = orderListModelList;
    }

    @NonNull
    @Override
    public TableReportAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_report_inflater, parent, false);
        return new TableReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TableReportAdapter.MyViewHolder holder, int position) {
        OrderListModel orderListModel = orderListModelList.get(position);
        if (orderListModel.getOrderTable().getOrder_customer_name().equals("")) {
            holder.tvOrderCustomerName.setText("Not mentioned");

        }else {
            holder.tvOrderCustomerName.setText(orderListModel.getOrderTable().getOrder_customer_name());

        }if (orderListModel.getOrderTable().getOrder_customer_mobile().equals("")) {
            holder.tvOrderCustomerMobileNo.setText("Not mentioned");

        }else {
            holder.tvOrderCustomerMobileNo.setText(orderListModel.getOrderTable().getOrder_customer_mobile());

        }
        holder.tvOrderTableNo.setText(orderListModel.getOrderTable().getOrder_table_number());
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat serverDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        try {
            holder.tvOrderDate.setText(""+simpleDateFormat.format(serverDateFormat.parse(orderListModel.getOrderTable().getOrder_date())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tvOrderTotalAmount.setText(""+orderListModel.getOrderTable().getOrder_total_amount());
        holder.llOrderItemContainer.removeAllViews();
        for (OrderItemTable orderItemTable:orderListModel.getOrderItemTableList()){
            final View child = activity.getLayoutInflater().inflate(R.layout.order_item__list_layout,null);
            TextView tvItemName = (TextView) child.findViewById(R.id.tvItemName);
            tvItemName.setText(orderItemTable.getOrder_item_name());
            TextView tvOrderQty = (TextView) child.findViewById(R.id.tvOrderQty);
            tvOrderQty.setText(orderItemTable.getOrder_item_qty() + "");
            TextView tvTotalAmount = (TextView) child.findViewById(R.id.tvTotalAmount);
            tvTotalAmount.setText("₹"+orderItemTable.getOrder_total_amount());
            holder.llOrderItemContainer.addView(child);
        }
    }

    @Override
    public int getItemCount() {
        return orderListModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvOrderCustomerName,tvOrderTableNo,tvOrderCustomerMobileNo,tvOrderDate,tvOrderTotalAmount;
        LinearLayout llOrderItemContainer;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvOrderCustomerName=(TextView)itemView.findViewById(R.id.tvOrderCustomerName);
            tvOrderCustomerMobileNo=(TextView)itemView.findViewById(R.id.tvOrderCustomerMobileNo);
            tvOrderDate = (TextView)itemView.findViewById(R.id.tvOrderDate);
            tvOrderTableNo = (TextView)itemView.findViewById(R.id.tvOrderTableNo);
            tvOrderTotalAmount = (TextView)itemView.findViewById(R.id.tvOrderTotalAmount);
            llOrderItemContainer=(LinearLayout)itemView.findViewById(R.id.llOrderItemContainer);

        }
    }
}
