package com.example.restaurantsbillmanager.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.database.ItemTable;

import java.util.List;

public class ItemSpinnerAdapter extends ArrayAdapter<ItemTable> {

    LayoutInflater flater;

    public ItemSpinnerAdapter(Activity context, int resouceId, int textviewId, List<ItemTable> list){

        super(context,resouceId,textviewId, list);
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ItemTable rowItem = getItem(position);
        View rowview = flater.inflate(R.layout.spinner_item_inflater,null,true);
        TextView txtTitle = (TextView) rowview.findViewById(R.id.tvSpinnerItem);
        txtTitle.setText(rowItem.getItem_name());
        if (position == 0) {
            // Set the hint text color gray
            txtTitle.setTextColor(Color.GRAY);
        } else {
            txtTitle.setTextColor(Color.BLACK);
        }
        return rowview;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = flater.inflate(R.layout.spinner_item_inflater,parent, false);
        }
        ItemTable rowItem = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.tvSpinnerItem);
        txtTitle.setText(rowItem.getItem_name());
        if (position == 0) {
            // Set the hint text color gray
            txtTitle.setTextColor(Color.GRAY);
        } else {
            txtTitle.setTextColor(Color.BLACK);
        }
        return convertView;
    }

}
