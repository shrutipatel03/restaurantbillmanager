package com.example.restaurantsbillmanager.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.database.ItemTable;
import com.example.restaurantsbillmanager.database.TableTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TableListAdapter extends RecyclerView.Adapter<TableListAdapter.MyViewHolder> implements Filterable {
    List<TableTable> tableTableList;
    List<TableTable> filterableTableList;
    Context context;
    TableListAdapter.OnTableEditOrDeleteListener onTableEditOrDeleteListener;

    public interface OnTableEditOrDeleteListener{
        void onEditButtonClicked(int position);
        void onDeleteButtonClicked(int position);
    }
    public TableListAdapter(Context context, List<TableTable> tableTableList, TableListAdapter.OnTableEditOrDeleteListener onTableEditOrDeleteListener)
    {

        this.context=context;
        filterableTableList = tableTableList;
        this.onTableEditOrDeleteListener=onTableEditOrDeleteListener;
        this.tableTableList=tableTableList;
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charsequence = charSequence.toString();
                if (charsequence.isEmpty()){
                    filterableTableList = tableTableList;
                }else {
                    List<TableTable> tableTables = new ArrayList<>();
                    for (TableTable row : tableTableList){
                        if (row.getTable_name().toLowerCase().contains(charsequence.toLowerCase())){
                            tableTables.add(row);
                        }
                    }
                    filterableTableList = tableTables;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filterableTableList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterableTableList = (ArrayList<TableTable>)filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    @Override
    public TableListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_name_inflater, parent, false);
        return new TableListAdapter.MyViewHolder(itemView);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTableName,ivDeleteTable;
        public ImageView ivUdateTable;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTableName = (TextView) itemView.findViewById(R.id.tvTableName);
            ivUdateTable=(ImageView)itemView.findViewById(R.id.ivTableUpdate);
            ivDeleteTable=(TextView) itemView.findViewById(R.id.ivTableDelete);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull TableListAdapter.MyViewHolder holder, int position) {
        TableTable data = filterableTableList.get(position);
        holder.tvTableName.setText(""+data.getTable_name());

        holder.ivUdateTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTableEditOrDeleteListener.onEditButtonClicked(position);
            }
        });
        holder.ivDeleteTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTableEditOrDeleteListener.onDeleteButtonClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filterableTableList.size();
    }
}
