package com.example.restaurantsbillmanager.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.database.ItemTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.MyViewHolder> implements Filterable {

    List<ItemTable> itemTableList;
    List<ItemTable> filterableItemList;
    Context context;
    OnItemEditOrDeleteListener onItemEditOrDeleteListener;
    Random random;
    String[] colour;
    public interface OnItemEditOrDeleteListener{
        public void onEditButtonClicked(int position);

        public void onDeleteButtonClicked(int position);
    }
    public ItemListAdapter(Context context,List<ItemTable> itemTableList,OnItemEditOrDeleteListener onItemEditOrDeleteListener)
    {
        colour = new String[]{"#DD5629","#E27A25","#EAAC19","#F7E602","#86BA3B","#12A98A","#258EC7","#0074BA","#382948","#00ff00","#0070f7","#ff9a00","#2c2c2c","#6db432","#142E54","#e51e2b","#050b2b","#a33a47","#151060","#322831","#241f20"};
        random = new Random();
        filterableItemList = itemTableList;
        this.context=context;
        this.onItemEditOrDeleteListener=onItemEditOrDeleteListener;
        this.itemTableList=itemTableList;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charsequence = charSequence.toString();
                if (charsequence.isEmpty()){
                    filterableItemList = itemTableList;
                }else {
                    List<ItemTable> itemTables = new ArrayList<>();
                    for (ItemTable row : itemTableList){
                        if (row.getItem_name().toLowerCase().contains(charsequence.toLowerCase())){
                            itemTables.add(row);
                        }
                    }
                    filterableItemList = itemTables;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filterableItemList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterableItemList = (ArrayList<ItemTable>)filterResults.values;
                notifyDataSetChanged();
            }
        };
    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_inflater, parent, false);
        return new MyViewHolder(itemView);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvItemName, tvItemPrice,ivDeleteItem;
        public ImageView ivUdateItem,ivItemIcon;
        public LinearLayout llOuterLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvItemName = (TextView) itemView.findViewById(R.id.tvItemName);
            tvItemPrice = (TextView) itemView.findViewById(R.id.tvItemPrice);
            ivUdateItem=itemView.findViewById(R.id.ivUdateItem);
            ivDeleteItem=itemView.findViewById(R.id.ivDeleteItem);
//            ivItemIcon=itemView.findViewById(R.id.ivItemIcon);
//            llOuterLayout=itemView.findViewById(R.id.llOuterLayout);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ItemTable data = filterableItemList.get(position);
        holder.tvItemName.setText(""+data.getItem_name());
        holder.tvItemPrice.setText(""+data.getItem_price());
        holder.ivUdateItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemEditOrDeleteListener.onEditButtonClicked(position);
            }
        });
        holder.ivDeleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemEditOrDeleteListener.onDeleteButtonClicked(position);
            }
        });

       //
        // holder.ivItemIcon.setColorFilter(Color.argb(random.nextInt(255), random.nextInt(255), 255, random.nextInt(255)));
//        GradientDrawable background = (GradientDrawable) holder.llOuterLayout.getBackground();
//        background.setColor(Color.parseColor(colour[random.nextInt(colour.length-1)]));

    }

    @Override
    public int getItemCount() {
        return filterableItemList.size();
    }
}
