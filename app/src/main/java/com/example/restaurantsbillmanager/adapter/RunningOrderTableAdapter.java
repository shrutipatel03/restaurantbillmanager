package com.example.restaurantsbillmanager.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.database.OrderItemTable;
import com.example.restaurantsbillmanager.database.OrderTable;
import com.example.restaurantsbillmanager.database.TableTable;
import com.example.restaurantsbillmanager.model.OrderListModel;
import com.example.restaurantsbillmanager.model.SaveOrderModel;

import java.util.ArrayList;
import java.util.List;

public class RunningOrderTableAdapter  extends RecyclerView.Adapter<RunningOrderTableAdapter.MyViewHolder> {

//    List<OrderTable> orderTableList;
    List<OrderListModel> orderListModelList;
    Context context;
    RunningOrderTableAdapter.OnTableClickListener onTableClickListener;

    public interface OnTableClickListener{
        void ontableClicked(int position);

    }

    public RunningOrderTableAdapter( Context context,List<OrderListModel> orderListModelList, OnTableClickListener onTableClickListener) {
        this.orderListModelList = orderListModelList;

        this.context = context;
        this.onTableClickListener = onTableClickListener;
    }

    @NonNull
    @Override
    public RunningOrderTableAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.running_order_table_inflater, parent, false);
        return new RunningOrderTableAdapter.MyViewHolder(itemView);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTableName;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTableName = (TextView) itemView.findViewById(R.id.tvTableName);


        }
    }

    @Override
    public void onBindViewHolder(@NonNull RunningOrderTableAdapter.MyViewHolder holder, int position) {

        OrderListModel data = orderListModelList.get(position);

        holder.tvTableName.setText(""+data.getOrderTable().getOrder_table_number());

        Log.d("Running Order Adapter","data"+data.getOrderTable().getOrder_table_number());

        holder.tvTableName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTableClickListener.ontableClicked(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return orderListModelList.size();
    }
}
