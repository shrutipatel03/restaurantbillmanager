package com.example.restaurantsbillmanager.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.database.TableTable;

import java.util.List;

public class TableNameSpinnerAdapter extends ArrayAdapter<TableTable> {
    LayoutInflater flater;

    public TableNameSpinnerAdapter(Activity context, int resouceId, int textviewId, List<TableTable> list){

        super(context,resouceId,textviewId, list);
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TableTable table = getItem(position);
        View rowview = flater.inflate(R.layout.spinner_table_name_inflater,null,true);
        TextView txtTitle = (TextView) rowview.findViewById(R.id.tvSpinnerTableName);
        txtTitle.setText(table.getTable_name());
        if (position == 0) {
            // Set the hint text color gray
            txtTitle.setTextColor(Color.GRAY);
        } else {
            txtTitle.setTextColor(Color.BLACK);
        }
        return rowview;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = flater.inflate(R.layout.spinner_table_name_inflater,parent, false);
        }
        TableTable table = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.tvSpinnerTableName);
        txtTitle.setText(table.getTable_name());
        if (position == 0) {
            // Set the hint text color gray
            txtTitle.setTextColor(Color.GRAY);
        } else {
            txtTitle.setTextColor(Color.BLACK);
        }
        return convertView;
    }

}
