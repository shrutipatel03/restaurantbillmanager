package com.example.restaurantsbillmanager.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.database.ItemTable;
import com.example.restaurantsbillmanager.database.OrderItemTable;
import com.example.restaurantsbillmanager.model.OrderListModel;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.MyViewHolder> {

    List<OrderListModel> orderListModelList;
    Activity activity;
    OnOrderItemChange onOrderItemChange;
    public interface OnOrderItemChange{
        void onItemClicked(int position);
    }
    public OrderListAdapter(Activity activity, List<OrderListModel> orderListModelList,OnOrderItemChange onOrderItemChange)
    {
        this.activity=activity;
        this.orderListModelList=orderListModelList;
        this.onOrderItemChange=onOrderItemChange;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_inflater, parent, false);
        return new MyViewHolder(itemView);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvOrderCustomerName, tvOrderCustomerMobileNo,tvOrderTableNo,tvOrderDate,tvOrderTotalAmount;
        public LinearLayout llOrderItemContainer;
        public Button btnEditOrder,btnDeleteOrder;
        ImageView btnPrintOrder;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            llOrderItemContainer=(LinearLayout)itemView.findViewById(R.id.llOrderItemContainer);
            tvOrderCustomerName = (TextView) itemView.findViewById(R.id.tvOrderCustomerName);
            tvOrderCustomerMobileNo = (TextView) itemView.findViewById(R.id.tvOrderCustomerMobileNo);
            tvOrderTableNo = (TextView) itemView.findViewById(R.id.tvOrderTableNo);
            tvOrderDate = (TextView) itemView.findViewById(R.id.tvOrderDate);
            tvOrderTotalAmount = (TextView) itemView.findViewById(R.id.tvOrderTotalAmount);
            btnEditOrder = itemView.findViewById(R.id.btnEditOrder);
          //  btnPrintOrder = itemView.findViewById(R.id.btnPrintOrder);
            btnDeleteOrder = itemView.findViewById(R.id.btnDeleteOrder);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        OrderListModel data = orderListModelList.get(position);
        if (data.getOrderTable().getOrder_customer_name().equals("")){
            holder.tvOrderCustomerName.setText("Not mentioned");

        }else{
            holder.tvOrderCustomerName.setText(""+data.getOrderTable().getOrder_customer_name());
        }
        if (data.getOrderTable().getOrder_customer_mobile().equals("")){
            holder.tvOrderCustomerMobileNo.setText("Not mentioned");

        }else {
            holder.tvOrderCustomerMobileNo.setText("" + data.getOrderTable().getOrder_customer_mobile());
        }

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat serverDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        try {
            holder.tvOrderDate.setText(""+simpleDateFormat.format(serverDateFormat.parse(data.getOrderTable().getOrder_date())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tvOrderTableNo.setText(""+data.getOrderTable().getOrder_table_number());
        holder.tvOrderTotalAmount.setText(""+data.getOrderTable().getOrder_total_amount()+"");

//        holder.llOrderItemContainer.removeAllViews();
      /*  for(OrderItemTable orderItemTable:data.getOrderItemTableList())
        {
            View child = activity.getLayoutInflater().inflate(R.layout.order_item__list_layout, null);
            TextView tvItemName=(TextView)child.findViewById(R.id.tvItemName);
            tvItemName.setText(orderItemTable.getOrder_item_name());
            TextView tvOrderQty=(TextView)child.findViewById(R.id.tvOrderQty);
            tvOrderQty.setText(orderItemTable.getOrder_item_qty()+"");
            TextView tvTotalAmount=(TextView)child.findViewById(R.id.tvTotalAmount);
            tvTotalAmount.setText(orderItemTable.getOrder_total_amount()+"");
            holder.llOrderItemContainer.addView(child);
        }*/
//        holder.btnEditOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onOrderItemChange.onEditClicked(position);
//            }
//        });
//
//        holder.btnPrintOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onOrderItemChange.onPrintClicked(position);
//            }
//        });
//
//        holder.btnDeleteOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onOrderItemChange.onDeleteClicked(position);
//            }
//        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOrderItemChange.onItemClicked(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        Log.d("OrderListAdapter",""+orderListModelList.size());
        return orderListModelList.size();
    }
}
