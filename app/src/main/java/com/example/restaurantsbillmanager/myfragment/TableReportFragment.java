package com.example.restaurantsbillmanager.myfragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.os.Environment;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.activity.UpdateRunningOrderActivity;
import com.example.restaurantsbillmanager.adapter.TableNameSpinnerAdapter;
import com.example.restaurantsbillmanager.adapter.TableReportAdapter;
import com.example.restaurantsbillmanager.database.MySharedPreference;
import com.example.restaurantsbillmanager.database.OrderItemTable;
import com.example.restaurantsbillmanager.database.OrderTable;
import com.example.restaurantsbillmanager.database.RestaurantDatabase;
import com.example.restaurantsbillmanager.database.TableTable;
import com.example.restaurantsbillmanager.model.OrderListModel;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.uttampanchasara.pdfgenerator.CreatePdf;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class TableReportFragment extends Fragment {

    @BindView(R.id.spTableName)
    SearchableSpinner spTableName;
    @BindView(R.id.etTableReportDate)
    EditText etTableReportDate;
    @BindView(R.id.rvTableReport)
    RecyclerView rvTableReport;
    List<TableTable> tableTableList;
    TableNameSpinnerAdapter tableSpinnerAdapter;
    RestaurantDatabase restaurantDatabase;
    SimpleDateFormat simpleDateFormat;
    SimpleDateFormat serverSimpleDateFormat;
    Calendar calendar;
    DatePickerDialog.OnDateSetListener dateSetListener;
    TableReportAdapter tableReportAdapter;
    List<OrderListModel> orderListModelList;
    private MenuItem item;
    MySharedPreference mySharedPreference;
    Date date;
    public TableReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_table_report, container, false);
        ButterKnife.bind(this, rootview);
        setHasOptionsMenu(true);
        initAllControls();
        return rootview;
    }

    private void initAllControls() {
        restaurantDatabase = Room.databaseBuilder(getContext(),
                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
        date = new Date();
        tableTableList = new ArrayList<>();
        orderListModelList = new ArrayList<>();
        mySharedPreference = new MySharedPreference(getContext());
        tableSpinnerAdapter = new TableNameSpinnerAdapter(getActivity(), R.layout.spinner_table_name_inflater, R.id.tvSpinnerTableName, tableTableList);
        serverSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        etTableReportDate.setText(format);
        calendar = Calendar.getInstance();
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLable();
            }

        };
        tableReportAdapter = new TableReportAdapter(getActivity(), orderListModelList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvTableReport.setLayoutManager(mLayoutManager);
        rvTableReport.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration itemDecor = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecor.setDrawable(getResources().getDrawable(R.drawable.line_divider));
        rvTableReport.addItemDecoration(itemDecor);
        rvTableReport.setAdapter(tableReportAdapter);
        getTableDataFromLocalDatabase();
    }

    private void getTableDataFromLocalDatabase() {
        new AsyncTask<Void, Void, List<TableTable>>() {
            @Override
            protected List<TableTable> doInBackground(Void... voids) {
                return restaurantDatabase.getTableDao().fetchAllTableData();
            }

            @Override
            protected void onPostExecute(List<TableTable> tableTableList) {
                super.onPostExecute(tableTableList);
                TableReportFragment.this.tableTableList.clear();
                TableTable tableTable = new TableTable("Select Table");
                TableReportFragment.this.tableTableList.add(tableTable);
                TableReportFragment.this.tableTableList.addAll(tableTableList);
                spTableName.setTitle("Select Table");
                spTableName.setPositiveButton("OK");
                spTableName.setAdapter(tableSpinnerAdapter);
                tableSpinnerAdapter.notifyDataSetChanged();
            }


        }.execute();
    }

    @OnClick(R.id.etTableReportDate)
    public void onDateClicked() {
        new DatePickerDialog(getContext(), dateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLable() {
        String format = "dd-MM-yyyy";
        simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        etTableReportDate.setText(simpleDateFormat.format(calendar.getTime()));
    }

    @OnClick(R.id.btnTableReportGo)
    public void onbtnTableReportGo() {
        String tableName = spTableName.getSelectedItem().toString();

        try {
            date =simpleDateFormat.parse(etTableReportDate.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (spTableName.getSelectedItemPosition() == 0) {
            Snackbar.make(spTableName, "please select table name", Snackbar.LENGTH_LONG).show();
            return;
        } else {
            getTableReportsFromDatabase(tableName,  serverSimpleDateFormat.format(date));
        }
    }

    private void getTableReportsFromDatabase(String tableName, String date) {
        new AsyncTask<Void, Void, List<OrderListModel>>() {
            @Override
            protected List<OrderListModel> doInBackground(Void... voids) {
                List<OrderTable> orderTableList = restaurantDatabase.getOrderTableDao().fetchTableAndDatewiseOrderTableData("finish", tableName, date);
                List<OrderListModel> orderListModelList = new ArrayList<>();
                for (OrderTable orderTable : orderTableList) {
                    OrderListModel orderListModel = new OrderListModel();
                    orderListModel.setOrderTable(orderTable);
                    orderListModel.setOrderItemTableList(restaurantDatabase.getOrderItemTableDao().fetchOrderIdWiseSubOrderData(orderTable.getOrder_id()));
                    orderListModelList.add(orderListModel);
                }
                return orderListModelList;
            }

            @Override
            protected void onPostExecute(List<OrderListModel> orderListModelList) {
                super.onPostExecute(orderListModelList);
                if (orderListModelList.size() > 0) {
                    rvTableReport.setVisibility(View.VISIBLE);
                    TableReportFragment.this.orderListModelList.clear();
                    TableReportFragment.this.orderListModelList.addAll(orderListModelList);
                    TableReportFragment.this.tableReportAdapter.notifyDataSetChanged();
                    item.setVisible(true);

                } else {
                    item.setVisible(false);
                    rvTableReport.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "No data Found", Toast.LENGTH_LONG).show();
                    TableReportFragment.this.tableReportAdapter.notifyDataSetChanged();
                }

            }
        }.execute();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.report_download, menu);
        item = menu.findItem(R.id.download_report);
        item.setVisible(false);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            if (item.getItemId()==R.id.download_report){
                if (orderListModelList != null) {
                    Dexter.withActivity(getActivity())
                            .withPermissions(
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE
                            ).withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            /* ... */
                            if (report.areAllPermissionsGranted()) {
                                DownloadPdf();
                                Toast.makeText(getContext(), "Downloading...", Toast.LENGTH_LONG).show();
                            } else {
                                showPermissionDailog();
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();
                } else {
                    Toast.makeText(getContext(), "no data found", Toast.LENGTH_LONG).show();
                }
            }
            return false;
        }
    private void DownloadPdf() {
        File directory = createDirectory();

        String content = "<html>" +
                "    <body>" +
                "<style>"+
                "    .invoice-box table tr.heading td {\n" +
                "        background: #F5B041;\n" +
                "        float: left;\n" +
                "       width: 25%;\n" +
                "        border-bottom: 1px solid #ddd;\n" +
                "        font-weight: bold;\n" +
                "    }\n" +
                "</style>"+
                "        <table width='100%' >" +
                "            <tbody>" +
                "                <tr>" +
//                "                    <th colspan='3'><h1><img border=\"0\" src=\"file:///android_res/drawable/about_us.png\" alt=\"nothing\" width=\"60\" height=\"60\" />" + mySharedPreference.getRestourantName() + "</h1></th>" +
                "                               <center><img border=\"0\" src=\"file:///android_res/drawable/appicon.png\" alt=\"nothing\" width=\"60\" height=\"60\" />" +
                "                                     <h1>"+mySharedPreference.getRestourantOwerName()+"</h1></center>"+

                "                </tr>" +
                "                <tr>" +
                "                    <td>" +
                "                        Table Name  :- " + spTableName.getSelectedItem().toString() +
                "                    </td>" +
                "                    <td align='right'>" +
                "                        Date :- " + etTableReportDate.getText().toString() +
                "                    </td>" +
                "                </tr>" +
                "            </tbody>" +
                "        </table>" +
                "        <hr>" +
                "        <table border='1' cellspacing='0' align='center' width='100%'>" +
                "            <tbody>" +
                "                <tr bgcolor=#F5B041>" +
                "                    <th align='center'>" +"Customer Name"+ "</th>" +
                "                    <th align='center'>" + "Customer Mobile" + "</th>" +
//                "                    <th>" + getString(R.string.mobile_number) + "</th>" +
                "                    <th align='center'>" + "Item Name" + "</th>" +
                "                    <th align='center'>" + "Item Quantity" + "</th>" +
                "                    <th align='center'>" + "Item Price" + "</th>" +
                "                    <th align='center'>" + "Total Amount" + "</th>" +
                "                    <th align='center'>" + "Bill" + "</th>" +
                "                </tr>";
        for (OrderListModel orderListModel : orderListModelList) {
            content += "                <tr>" +
                    "                    <td align='center'>" + orderListModel.getOrderTable().getOrder_customer_name() + "</td>" +
                    "                    <td align='center'>" + orderListModel.getOrderTable().getOrder_customer_mobile() + "</td>" ;

                content +=
                        "                    <td align='center'>";
            for (OrderItemTable orderItemTable: orderListModel.getOrderItemTableList()){
                content +=         orderItemTable.getOrder_item_name()  ;
                content +=            "<br>" ;
                        }

            content +=            "</td>\n" ;

            content +=
                    "                    <td align='center'>";
            for (OrderItemTable orderItemTable: orderListModel.getOrderItemTableList()){
                content +=         orderItemTable.getOrder_item_qty()  ;
                content +=            "<br>" ;
            }

            content +=            "</td>\n" ;

            content +=
                    "                    <td align='center'>";
            for (OrderItemTable orderItemTable: orderListModel.getOrderItemTableList()){
                content +=         orderItemTable.getOrder_item_price()  ;
                content +=            "<br>" ;
            }

            content +=            "</td>\n" ;

            content +=
                    "                    <td align='center'>";
            for (OrderItemTable orderItemTable: orderListModel.getOrderItemTableList()){
                content +=         orderItemTable.getOrder_total_amount()  ;
                content +=            "<br>" ;
            }

            content +=            "</td>\n" ;

     content +=       "                    <td align='center'>" + orderListModel.getOrderTable().getOrder_total_amount() + "</td>" ;

                       /* "                    <td align='center'>" + orderItemTable.getOrder_item_qty() + "</td>" +
                        "                    <td align='center'>" + orderItemTable.getOrder_item_price()+ "</td>"+
                        "                    <td align='center'>" + orderItemTable.getOrder_total_amount()+ "</td>"*/;


               content+=     "                </tr>";
        }

        content += "            </tbody>" +
                "        </table>" +
                "    </body>" +
                "</html>";
        new CreatePdf(getContext())
                .setPdfName("Table_report_" + mySharedPreference.getRestourantName() + "_" + etTableReportDate.getText().toString()+"_"+spTableName.getSelectedItem().toString())
                .openPrintDialog(false)
                .setContentBaseUrl(null)
                .setContent(content)
                .setFilePath(directory.getAbsolutePath() + File.separator)
                .setCallbackListener(new CreatePdf.PdfCallbackListener() {
                    @Override
                    public void onFailure(String s) {
                        Toast.makeText(getContext(),"failed download", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(String s) {
                        String file_path=directory.getAbsolutePath() + File.separator+"General_report_" + mySharedPreference.getRestourantName() + "_" + etTableReportDate.getText().toString()+"_"+spTableName.getSelectedItem().toString();
                        Toast.makeText(getContext(), "download complete "+file_path, Toast.LENGTH_SHORT).show();
                    }
                })
                .create();

    }

    private File createDirectory() {
        File mFile;
        if (isExternalStorageWritable()) {
            mFile = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "restaurant_bill_manager");
        } else {
            mFile = new File(Environment.getDataDirectory().toString() + File.separator + "restaurant_bill_manager");
        }

        if (!mFile.exists())
            mFile.mkdirs();
        return mFile;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public void showPermissionDailog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Need Send SMS Permission");
        builder.setMessage("This app needs SMS permission for send please give permission from settings.");
        builder.setPositiveButton("Go To Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

}
