package com.example.restaurantsbillmanager.myfragment;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.restaurantsbillmanager.R;
//import com.example.restaurantsbillmanager.activity.AddTableActivity;
//import com.example.restaurantsbillmanager.activity.UpdateOrderActivity;
import com.example.restaurantsbillmanager.activity.UpdateRunningOrderActivity;
import com.example.restaurantsbillmanager.adapter.RunningOrderTableAdapter;
import com.example.restaurantsbillmanager.adapter.TableListAdapter;
import com.example.restaurantsbillmanager.database.OrderTable;
import com.example.restaurantsbillmanager.database.RestaurantDatabase;
import com.example.restaurantsbillmanager.model.OrderListModel;
import com.example.restaurantsbillmanager.myUtils.DeleteConfirmDialogListener;
import com.example.restaurantsbillmanager.myUtils.GlobalMethods;

import java.util.ArrayList;
import java.util.List;
import java.util.TooManyListenersException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewRunningOrderFragment extends Fragment {

    @BindView(R.id.rvTableList)
    RecyclerView rvTableList;
    RestaurantDatabase restaurantDatabase;
    RunningOrderTableAdapter runningOrderTableAdapter;
//    List<OrderTable> orderTableList;
    List<OrderListModel> orderListModelList;
    OrderTable orderTable;
    OrderListModel orderListModel;
    String status;
    public ViewRunningOrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view_running_order, container, false);
        ButterKnife.bind(this,rootView);
        initAllControls();
        return rootView;
    }

    private void initAllControls() {
//        orderListModel = getArguments().getParcelable("data");
        restaurantDatabase = Room.databaseBuilder(getContext(),
                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
        orderListModelList = new ArrayList<>();
//        orderListModelList.add(orderListModel);
        runningOrderTableAdapter = new RunningOrderTableAdapter(getContext(), orderListModelList, new RunningOrderTableAdapter.OnTableClickListener() {

            @Override
            public void ontableClicked(int position) {
                Intent intent = new Intent(getContext(), UpdateRunningOrderActivity.class);
                intent.putExtra("data", orderListModelList.get(position));
               // intent.putExtra("position", position);
                startActivityForResult(intent,208);
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvTableList.setLayoutManager(mLayoutManager);
        rvTableList.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration itemDecor = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecor.setDrawable(getResources().getDrawable(R.drawable.line_divider));
        rvTableList.addItemDecoration(itemDecor);
        rvTableList.setAdapter(runningOrderTableAdapter);

        getRunningOrderData();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==208 && resultCode== Activity.RESULT_OK)
        {

            getRunningOrderData();
        }
    }
    private void getRunningOrderData() {
        new AsyncTask<Void,Void, List<OrderListModel>>(){

            @Override
            protected List<OrderListModel> doInBackground(Void... voids) {

                ViewRunningOrderFragment.this.orderListModelList.clear();
                List<OrderTable> orderTableList = restaurantDatabase.getOrderTableDao().getStatus("active");

                List<OrderListModel> orderListModelList = new ArrayList<>();
                for (OrderTable orderTable:orderTableList){
                    OrderListModel orderListModel = new OrderListModel();
                    orderListModel.setOrderTable(orderTable);
                    orderListModel.setOrderItemTableList(restaurantDatabase.getOrderItemTableDao().fetchOrderIdWiseSubOrderData(orderTable.getOrder_id()));
                    orderListModelList.add(orderListModel);
                }
                return orderListModelList;
            }

            @Override
            protected void onPostExecute(List<OrderListModel> orderListModelList) {
                super.onPostExecute(orderListModelList);
                if (orderListModelList.size()>0) {
                    ViewRunningOrderFragment.this.orderListModelList.clear();
                    ViewRunningOrderFragment.this.orderListModelList.addAll(orderListModelList);
                    runningOrderTableAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(getContext(),"No data Found",Toast.LENGTH_LONG).show();
                    runningOrderTableAdapter.notifyDataSetChanged();
                }
            }
        }.execute();
    }
}
