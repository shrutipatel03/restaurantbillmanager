package com.example.restaurantsbillmanager.myfragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.restaurantsbillmanager.R;
//import com.example.restaurantsbillmanager.activity.AddItemActivity;
//import com.example.restaurantsbillmanager.activity.HomeActivity;
import com.example.restaurantsbillmanager.adapter.ItemListAdapter;
import com.example.restaurantsbillmanager.database.ItemTable;
import com.example.restaurantsbillmanager.database.RestaurantDatabase;
import com.example.restaurantsbillmanager.myUtils.DeleteConfirmDialogListener;
import com.example.restaurantsbillmanager.myUtils.GlobalMethods;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ItemListFragment extends Fragment {


    RestaurantDatabase restaurantDatabase;
    String TAG = "ItemListFragment";
    @BindView(R.id.rvItemList)
    RecyclerView rvItemList;
    @BindView(R.id.etSearchItem)
    EditText etSearchItem;
    List<ItemTable> itemTableList;
    ItemListAdapter itemListAdapter;
    String[] colour;
    ItemTable itemTable;
    //    @BindView(R.id.tvMainTitle)
//    TextView tvMainTitle;
    @BindView(R.id.etItemName)
    EditText etItemName;
    @BindView(R.id.etItemPrice)
    EditText etItemPrice;
    @BindView(R.id.btnSaveItem)
    Button btnSaveItem;

    @BindView(R.id.tvAddItems)
    TextView tvAddItems;
    @BindView(R.id.ivclose)
    ImageView ivClose;
    @BindView(R.id.lladdItem)
    RelativeLayout lladdItem;
    @BindView(R.id.imgSlideup)
    ImageView imgSlideup;

    int rotation=0;
    int a;
    BottomSheetBehavior<RelativeLayout> behavior;
    public ItemListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_item_list, container, false);
        ButterKnife.bind(this, rootView);


        initAllControls();
        return rootView;
    }

    public void initAllControls() {


        behavior = BottomSheetBehavior.from(lladdItem);
        behavior.setPeekHeight(150);




        //  behavior.getPeekHeight();

//        Log.d("ItemList","height"+behavior.getPeekHeight());
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//         itemTable = getArguments().getParcelable("data");
        if (getActivity().getIntent().hasExtra("data")){
//            getSupportActionBar().setTitle("Update Item");

//            itemTable = getActivity().getIntent().getParcelableExtra("data");

        }else {
//            getSupportActionBar().setTitle("Add Item");
            btnSaveItem.setText("Add");
        }

        colour = new String[]{"#DD5629","#E27A25","#EAAC19","#F7E602","#86BA3B","#12A98A","#258EC7","#0074BA","#382948","#00ff00"};
        itemTableList = new ArrayList<>();
        itemListAdapter = new ItemListAdapter(getContext(), itemTableList, new ItemListAdapter.OnItemEditOrDeleteListener() {
            @Override
            public void onEditButtonClicked(int position) {
//                Intent intent = new Intent(getContext(), AddItemActivity.class);
//                intent.putExtra("data", itemTableList.get(position));
//                startActivityForResult(intent,208);

                 itemTable=itemTableList.get(position);
                etItemName.setText(itemTable.getItem_name());
                etItemPrice.setText(""+itemTable.getItem_price());
                btnSaveItem.setText("Update");
                tvAddItems.setText("Update Item");
//                behavior.setFitToContents(true);
//                behavior.setPeekHeight(-1);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                ivClose.setVisibility(View.VISIBLE);
//                rotation = rotation + 180;
//                imgSlideup.setRotation(rotation);
                //itemTableList.get(position).setItem_name(etItemName.getText().toString());
//                itemTableList.get(position).setItem_price(Double.parseDouble(etItemPrice.getText().toString()));
//                updateItemName(etItemName.getText().toString(),etItemPrice.getText().toString());
            }

            @Override
            public void onDeleteButtonClicked(int position) {
                GlobalMethods.showDeleteConfirmDialog(getContext(), new DeleteConfirmDialogListener() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        deleteDataFromLocalDatabase(position);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });
            }
        });
        etSearchItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                itemListAdapter.getFilter().filter(editable.toString());
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvItemList.setLayoutManager(mLayoutManager);
        rvItemList.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration itemDecor = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecor.setDrawable(getResources().getDrawable(R.drawable.line_divider));
        rvItemList.addItemDecoration(itemDecor);
        rvItemList.setAdapter(itemListAdapter);
        restaurantDatabase = Room.databaseBuilder(getContext(),
                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();

        getDataFromApi();
        //HomeActivity.getInstance().ivAddData.setVisibility(View.VISIBLE);


    }

    @OnClick(R.id.ivclose)
    public void onClose(){
//        behavior.setPeekHeight(150);
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        onClearData();
    }

    public void onClearData(){
        tvAddItems.setText("Add Item");
        btnSaveItem.setText("Add");
        etItemName.setText("");
        etItemPrice.setText("");
        ivClose.setVisibility(View.INVISIBLE);
//        imgSlideup.setRotation(0);
    }
//    @OnTouch(R.id.lladdItem)
//    public  void  onTouchlladdItem(){
//        if (behavior.getState()>=BottomSheetBehavior.STATE_EXPANDED){
//            rotation = rotation + 180;
//            imgSlideup.setRotation(rotation);
//        }else {
//            imgSlideup.setRotation(0);
//        }

//        if(behavior.setState(BottomSheetBehavior.STATE_EXPANDED)){
//            rotation=rotation+180;
//            imgSlideup.setRotation(rotation);
//            //   imgSlideup.setVisibility(View.GONE);
//        }else {
//            rotation=rotation-180;
//            imgSlideup.setRotation(rotation);
//        }
//    }






//    @OnClick(R.id.fabAddItem)
//    public void onbtnAddItem(){
//        Intent intent = new Intent(getContext(),AddItemActivity.class);
//        startActivityForResult(intent,208);
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==208 && resultCode== Activity.RESULT_OK)
        {
            getDataFromApi();
        }
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        if(HomeActivity.getInstance()!=null && HomeActivity.getInstance().tvMainTitle!=null)
//        {
//            HomeActivity.getInstance().tvMainTitle.setText("Item List");
//        }
//    }

    public void deleteDataFromLocalDatabase(int position)
    {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                int delete=restaurantDatabase.getItemTableDao().deleteItemTableData(itemTableList.get(position));
                if(delete>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if(aBoolean)
                {
                    Toast.makeText(getContext(), "Successfully Delete Item", Toast.LENGTH_SHORT).show();
                    getDataFromApi();
                }
                else
                {
                    Toast.makeText(getContext(), "Failed Delete Item", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    public void getDataFromApi() {
        new AsyncTask<Void, Void, List<ItemTable>>() {
            @Override
            protected List<ItemTable> doInBackground(Void... voids) {
                return restaurantDatabase.getItemTableDao().fetchAllItemTableData();
            }

            @Override
            protected void onPostExecute(List<ItemTable> itemTables) {
                super.onPostExecute(itemTables);
                Log.d(TAG, "onPostExecute: " + itemTables.size());
                itemTableList.clear();
                itemTableList.addAll(itemTables);
                itemListAdapter.notifyDataSetChanged();
            }
        }.execute();
    }

    @OnClick(R.id.btnSaveItem)
    public void onItemButtonClicked(View view) {

//        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        String itemName=etItemName.getText().toString();
        String price=etItemPrice.getText().toString();
        if(itemName.trim().length()==0)
        {
            Toast.makeText(getContext(),"Please Enter Item Name",Toast.LENGTH_SHORT).show();
        }
        else if(price.trim().length()==0)
        {
            Toast.makeText(getContext(),"Please Enter Item Price",Toast.LENGTH_SHORT).show();
        }
        else
        {
            if (tvAddItems.getText().toString().equals("Update Item")){
                updateItemName(itemName,price);
            }else {
                saveItemName(itemName, price);
            }
        }
    }

    public void saveItemName(String itemName,String itemPrice)
    {
        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {

                ItemTable itemTable;
                try {
                    itemTable = new ItemTable(itemName,Double.parseDouble(itemPrice));
                }catch (NumberFormatException ex)
                {
                    itemTable = new ItemTable(itemName,0);
                }

                return restaurantDatabase.getItemTableDao().insertItemTable(itemTable);
            }

            @Override
            protected void onPostExecute(Long aLong) {
                super.onPostExecute(aLong);
                if(aLong>0)
                {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    Toast.makeText(getContext(), "Insert Successfully", Toast.LENGTH_SHORT).show();
                    onClearData();
                    getDataFromApi();
                }
                else
                {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    onClearData();
                    Toast.makeText(getContext(), "Insert Failed", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }
    public void updateItemName(String itemName,String itemPrice)
    {
        new AsyncTask<Void, Void,Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {


                try {
                    //itemTable = new ItemTable(itemName,Long.parseLong(itemPrice));
                    itemTable.setItem_name(itemName);
                    itemTable.setItem_price(Double.parseDouble(itemPrice));
                }catch (NumberFormatException ex)
                {
                    itemTable.setItem_name(itemName);
                    itemTable.setItem_price(0);
                    //itemTable = new ItemTable(itemName,0);
                }

                int update= restaurantDatabase.getItemTableDao().updateItemTableData(itemTable);
                if(update>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean aLong) {
                super.onPostExecute(aLong);
                if(aLong)
                {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    Toast.makeText(getContext(), "Update Successfully", Toast.LENGTH_SHORT).show();
                    onClearData();
                    getDataFromApi();
//                    finish();
                }
                else
                {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    onClearData();
                    Toast.makeText(getContext(), "Update Failed", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }
}
