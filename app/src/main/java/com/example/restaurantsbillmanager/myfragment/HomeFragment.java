package com.example.restaurantsbillmanager.myfragment;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.room.Room;

import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.database.OrderTable;
import com.example.restaurantsbillmanager.database.RestaurantDatabase;
import com.example.restaurantsbillmanager.model.OrderListModel;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeFragment extends Fragment {

    @BindView(R.id.tvTotalOrder)
    TextView tvTotalOrder;
    @BindView(R.id.tvTotalEarning)
    TextView tvTotalEarning;
    @BindView(R.id.tvTotalMonthOrder)
    TextView tvTotalMonthOrder;
    @BindView(R.id.tvTotalMonthEarning)
    TextView tvTotalMonthEarning;
    @BindView(R.id.etMonth)
    EditText etMonth;
    @BindView(R.id.etDailyDate)
    EditText etDailyDate;
    Calendar calendar;
    YearMonthPickerDialog yearMonthPickerDialog;
    RestaurantDatabase restaurantDatabase;
    SimpleDateFormat simpleMonthFormat;
    SimpleDateFormat serverDateFormat;
    SimpleDateFormat simpleDateFormat;
    DatePickerDialog.OnDateSetListener dateSetListener;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,root);
        initAllControls();
        return root;
    }

    private void initAllControls() {
        restaurantDatabase = Room.databaseBuilder(getContext(),
                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
        calendar = Calendar.getInstance();
         serverDateFormat = new SimpleDateFormat("yyyy-MM-dd");
         simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String DailyDateformat =new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        etDailyDate.setText(DailyDateformat);

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDailyDate();
            }

        };

        String format = new SimpleDateFormat("MMMM-yyyy", Locale.getDefault()).format(new Date());
        etMonth.setText(format);



        String month = new SimpleDateFormat("MM").format(calendar.getTime());
        String year = new SimpleDateFormat("yyyy").format(calendar.getTime());

        int months = Integer.parseInt(month)-1;
        int years = Integer.parseInt(year);
        String date = String.valueOf(1);
        calendar.set(years,months,1);

        int maxDate = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        String maximumDate = String.valueOf(maxDate);

        String fromDate = year+"-"+month+"-0"+date;
        String toDate = year+"-"+month+""+maximumDate;
        yearMonthPickerDialog = new YearMonthPickerDialog(getContext(), new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month) {

                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                updateLable();
                SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy");

                etMonth.setText(format.format(calendar.getTime()));
            }
        });

        try {
            getDataFromDatabase(serverDateFormat.format(simpleDateFormat.parse(DailyDateformat)));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        getMonthTotal(fromDate,toDate);

    }
    @OnClick(R.id.etDailyDate)
    public void onDailyDate(){
        new DatePickerDialog(getContext(), dateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateDailyDate(){
        String format = "dd-MM-yyyy";
        simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        etDailyDate.setText(simpleDateFormat.format(calendar.getTime()));

        try {
            getDataFromDatabase(serverDateFormat.format(simpleDateFormat.parse(simpleDateFormat.format(calendar.getTime()))));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
    @OnClick(R.id.etMonth)
    public void onivSelectDate(View view){
        yearMonthPickerDialog.show();
    }

    private void updateLable() {
        String format = "MMMM-yyyy";
        simpleMonthFormat = new SimpleDateFormat(format, Locale.getDefault());
        etMonth.setText(simpleMonthFormat.format(calendar.getTime()));
        String month = new SimpleDateFormat("MM").format(calendar.getTime());
        String year = new SimpleDateFormat("yyyy").format(calendar.getTime());

        int months = Integer.parseInt(month)-1;
        int years = Integer.parseInt(year);
        String date = String.valueOf(1);
        calendar.set(years,months,1);

        int maxDate = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        String maximumDate = String.valueOf(maxDate);

        String fromDate = year+"-"+month+"-0"+date;
        String toDate = year+"-"+month+""+maximumDate;

        getMonthTotal(fromDate,toDate);
    }

    private void getMonthTotal(String fromDate, String toDate) {
        new AsyncTask<Void, Void, List<OrderListModel>>() {
            @Override
            protected List<OrderListModel> doInBackground(Void... voids) {
//                HomeFragment.this.orderListModelList.clear();
                List<OrderTable> orderTableList= restaurantDatabase.getOrderTableDao().fetchtoDateOrFromDateOrderTableData("finish",fromDate,toDate);
                List<OrderListModel> orderListModelList = new ArrayList<>();
                for(OrderTable order:orderTableList)
                {
                    OrderListModel orderListModel=new OrderListModel();
                    orderListModel.setOrderTable(order);
                    orderListModel.setOrderItemTableList(restaurantDatabase.getOrderItemTableDao().fetchOrderIdWiseSubOrderData(order.getOrder_id()));
                    orderListModelList.add(orderListModel);
                }

                return orderListModelList;
            }

            @Override
            protected void onPostExecute(List<OrderListModel> orderListModelList) {
                super.onPostExecute(orderListModelList);
                Double earnings=0.0;
                for (OrderListModel orderListModel:orderListModelList){
                    earnings += orderListModel.orderTable.getOrder_total_amount();
                }
                tvTotalMonthEarning.setText(""+earnings);
                tvTotalMonthOrder.setText(""+orderListModelList.size());
            }
        }.execute();
    }



    public void getDataFromDatabase(String date)
    {
        new AsyncTask<Void, Void, List<OrderListModel>>() {
            @Override
            protected List<OrderListModel> doInBackground(Void... voids) {
//                HomeFragment.this.orderListModelList.clear();
                List<OrderTable> orderTableList= restaurantDatabase.getOrderTableDao().fetchTodayData("finish",date);
                List<OrderListModel> orderListModelList = new ArrayList<>();
                for(OrderTable order:orderTableList)
                {
                    OrderListModel orderListModel=new OrderListModel();
                    orderListModel.setOrderTable(order);
                    orderListModel.setOrderItemTableList(restaurantDatabase.getOrderItemTableDao().fetchOrderIdWiseSubOrderData(order.getOrder_id()));
                    orderListModelList.add(orderListModel);
                }

                return orderListModelList;
            }

            @Override
            protected void onPostExecute(List<OrderListModel> orderListModelList) {
                super.onPostExecute(orderListModelList);
                Double earnings=0.0;
                for (OrderListModel orderListModel:orderListModelList){
                    earnings += orderListModel.orderTable.getOrder_total_amount();
                }
                tvTotalEarning.setText(""+earnings);
                tvTotalOrder.setText(""+orderListModelList.size());
            }
        }.execute();
    }

}
