package com.example.restaurantsbillmanager.myfragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.database.ItemTable;
import com.example.restaurantsbillmanager.database.OrderItemTable;
import com.example.restaurantsbillmanager.database.OrderTable;
import com.example.restaurantsbillmanager.database.RestaurantDatabase;
import com.example.restaurantsbillmanager.database.TableTable;
//import com.example.restaurantsbillmanager.model.AllTableModel;
import com.example.restaurantsbillmanager.model.OrderListModel;
import com.example.restaurantsbillmanager.model.SaveOrderModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.channels.FileChannel;
import java.nio.channels.ScatteringByteChannel;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RestoreBackupFragment extends Fragment {

    @BindView(R.id.btnBackUp)
    Button btnBackUp;
    RestaurantDatabase restaurantDatabase;
//    List<AllTableModel> allTableModelList;
    List<TableTable> tableTableList;
    List<ItemTable> itemTableList;
    List<OrderTable> orderTableList;
    List<OrderItemTable> orderItemTableList;
    List<OrderListModel> orderListModelList;

    public RestoreBackupFragment() throws FileNotFoundException {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_table_restore_backup,null,false);
        ButterKnife.bind(this,view);
        initAll();
        return view;
    }

    private void initAll() {
        restaurantDatabase = Room.databaseBuilder(getContext(),
                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();

//        allTableModelList=new ArrayList<>();
        itemTableList=new ArrayList<>();
        tableTableList=new ArrayList<>();
        orderItemTableList=new ArrayList<>();
        orderTableList=new ArrayList<>();
        orderListModelList=new ArrayList<>();







        // Transfer bytes from the inputfile to the outputfile



    }
//
    @OnClick(R.id.btnBackUp)
    public void onClickbtnBackUp(View view){
      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new ExportDatabaseCSVTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            new ExportDatabaseCSVTask().execute();
        }*/

      export();

        //        try {
//            final String inFileName = "/data/data/com.example.restaurantsbillmanager/databases/restorant_db";
//            File dbFile = new File(inFileName);
//            FileInputStream fis = null;
//            fis = new FileInputStream(dbFile);
//            String outFileName = Environment.getExternalStorageDirectory()+"/database_copy";
//
//            // Open the empty db as the output stream
//            OutputStream output = null;
//            output = new FileOutputStream(outFileName);
//            byte[] buffer = new byte[1024];
//            int length = 0;
//            while (true) {
//                try {
//                    if (!((length = fis.read(buffer)) > 0)) break;
//                    output.write(buffer, 0, length);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            }
//            try {
//                output.flush();
//                output.close();
//                fis.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
    }

    public  void BackupDatabase() throws IOException
    {


     }


    public void export(){

        getBackUpData();



    }

    private void getBackUpData() {

        GetBackUpData getBackUpDat=new GetBackUpData();
        getBackUpDat.execute();
    }

    private class GetBackUpData extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... voids) {

            List<TableTable> table = restaurantDatabase.getTableDao().fetchAllTableData();
            List<ItemTable> item = restaurantDatabase.getItemTableDao().fetchAllItemTableData();
            List<OrderItemTable> orderItem = restaurantDatabase.getOrderItemTableDao().fetchAllOrderItemData();
            List<OrderTable> orderTables = restaurantDatabase.getOrderTableDao().fetchAllOrderTableData();

//            List<OrderItemTable> orderItemByOrderId = restaurantDatabase.getOrderItemTableDao().fetchOrderIdWiseSubOrderData(orderTables.g)
//            List<AllTableModel> temp = new ArrayList<>();
            for (TableTable tableTable:table){
                tableTableList.add(tableTable);
            }
            for (ItemTable itemTable:item){
                itemTableList.add(itemTable);
            }
            for (OrderTable orderTable:orderTables){
                OrderListModel orderListModel= new OrderListModel();
                orderListModel.setOrderTable(orderTable);
                orderListModel.setOrderItemTableList(restaurantDatabase.getOrderItemTableDao().fetchOrderIdWiseSubOrderData(orderTable.getOrder_id()));
                orderListModelList.add(orderListModel);
                orderTableList.add(orderTable);

            }
            for (OrderItemTable orderItemTable:orderItem){
                orderItemTableList.add(orderItemTable);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

           // allTableModelList.addAll(allTableModels);
            StringBuilder builder=new StringBuilder();
            builder.append("Id,Name");  /* String[] mySecondStringArray ={String.valueOf(user.get(i).getTable_id()), user.get(i).getTable_name()};
            csvWrite.writeNext(mySecondStringArray);*/


            for(int i=0; i<tableTableList.size(); i++){

                builder.append("\n"+String.valueOf(tableTableList.get(i).getTable_id())+","+tableTableList.get(i).getTable_name());

            }

            builder.append("\n\nItem Id,Item Name,Price");
            for(int i=0; i<itemTableList.size(); i++){

                builder.append("\n"+itemTableList.get(i).getItem_id()+","+itemTableList.get(i).getItem_name()+","+itemTableList.get(i).getItem_price());

            }
//            builder.append("\n\nOrder id,Customer Name,Customer Mobile,Table Name,Date,Bill");
//            for(int i=0; i<orderTableList.size(); i++){
//
//                builder.append("\n"+orderTableList.get(i).getOrder_id()+","+orderTableList.get(i).getOrder_customer_name()+","+orderTableList.get(i).getOrder_customer_mobile()+","+orderTableList.get(i).getOrder_table_number()+","+orderTableList.get(i).getOrder_date()+","+orderTableList.get(i).getOrder_total_amount());
//
//            }
//            builder.append("\n\nOrder id,Customer Name,Customer Mobile,Table Name,Date,Item Name,Quantity,Item Price,Total Amount,Bill");
//            for(int i=0; i<orderListModelList.size(); i++){
//
//                builder.append("\n"+orderListModelList.get(i).getOrderTable().getOrder_id()+","+orderListModelList.get(i).getOrderTable().getOrder_customer_name()+","+orderTableList.get(i).getOrder_customer_mobile()+","+orderListModelList.get(i).getOrderTable().getOrder_table_number()+","+orderListModelList.get(i).getOrderTable().getOrder_date()+","+orderListModelList.get(i).getOrderTable().getOrder_total_amount());
//
//            }
            builder.append("\n\nItem id,Item Name,Item Quantity,Item Price,Total Amount");
            for(int i=0; i<orderItemTableList.size(); i++){

                builder.append("\n"+orderItemTableList.get(i).getItem_id()+","+orderItemTableList.get(i).getOrder_item_name()+","+orderItemTableList.get(i).getOrder_item_qty()+","+orderItemTableList.get(i).getOrder_item_price()+","+orderItemTableList.get(i).getOrder_total_amount());

            }

            try{
                FileOutputStream fileOutputStream=getContext().openFileOutput("user.csv", Context.MODE_PRIVATE);
                fileOutputStream.write(builder.toString().getBytes());
                fileOutputStream.close();

                Context context=getContext();
          /*  File exportDir = new File(Environment.getExternalStorageDirectory(), "/Roomcsv/");
            if (!exportDir.exists()) { exportDir.mkdirs(); }

            File file = new File(exportDir, "Users.csv");*/
                File file=new File(getContext().getFilesDir(),"user.csv");
                Uri path= FileProvider.getUriForFile(context,"com.example.restaurantsbillmanager.fileprovider",file);
                Intent intent=new Intent(Intent.ACTION_SEND);
                intent.setType("text/csv");
                intent.putExtra(Intent.EXTRA_SUBJECT,"Data");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra(Intent.EXTRA_STREAM,path);
                startActivity(Intent.createChooser(intent,"send mail"));
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }

  /*  public class ExportDatabaseCSVTask extends AsyncTask<String, Void, Boolean> {
        private final ProgressDialog dialog = new ProgressDialog(getContext());
        //DBHelper dbhelper;
        private RestaurantDatabase restaurantDatabase;

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Exporting database...");
            this.dialog.show();
            //dbhelper = new DBHelper(MainActivity.this);
            restaurantDatabase = RestaurantDatabase.getRestaurantDatabase(getContext());
        }

        protected Boolean doInBackground(final String... args) {

            File exportDir = new File(Environment.getExternalStorageDirectory(), "/Roomcsv/");
            if (!exportDir.exists()) { exportDir.mkdirs(); }

            File file = new File(exportDir, "Users.csv");
            try {
                file.createNewFile();
                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                String[] column = {"Id", "Name"};
                csvWrite.writeNext(column);
                List<TableTable> user = restaurantDatabase.getTableDao().fetchAllTableData();
                for(int i=0; i<user.size(); i++){
                    String[] mySecondStringArray ={String.valueOf(user.get(i).getTable_id()), user.get(i).getTable_name()};
                    csvWrite.writeNext(mySecondStringArray);
                }
                csvWrite.close();
                return true;
            } catch (IOException e) {
                return false;
            }
        }
        protected void onPostExecute(final Boolean success) {
            if (this.dialog.isShowing()) { this.dialog.dismiss(); }
            if (success) {
                Toast.makeText(getContext(), "Export successful!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Export failed", Toast.LENGTH_SHORT).show();
            }
        }
    }
    public class CSVWriter {
        private PrintWriter pw;
        private char separator;
        private char escapechar;
        private String lineEnd;
        private char quotechar;
        public static final char DEFAULT_SEPARATOR = ',';
        public static final char NO_QUOTE_CHARACTER = '\u0000';
        public static final char NO_ESCAPE_CHARACTER = '\u0000';
        public static final String DEFAULT_LINE_END = "\n";
        public static final char DEFAULT_QUOTE_CHARACTER = '"';
        public static final char DEFAULT_ESCAPE_CHARACTER = '"';
        public CSVWriter(Writer writer) {
            this(writer, DEFAULT_SEPARATOR, DEFAULT_QUOTE_CHARACTER,
                    DEFAULT_ESCAPE_CHARACTER, DEFAULT_LINE_END);
        }
        public CSVWriter(Writer writer, char separator, char quotechar, char escapechar, String lineEnd) {
            this.pw = new PrintWriter(writer);
            this.separator = separator;
            this.quotechar = quotechar;
            this.escapechar = escapechar;
            this.lineEnd = lineEnd;
        }
        public void writeNext(String[] nextLine) {
            if (nextLine == null)
                return;
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < nextLine.length; i++) {

                if (i != 0) {
                    sb.append(separator);
                }
                String nextElement = nextLine[i];
                if (nextElement == null)
                    continue;
                if (quotechar != NO_QUOTE_CHARACTER)
                    sb.append(quotechar);
                for (int j = 0; j < nextElement.length(); j++) {
                    char nextChar = nextElement.charAt(j);
                    if (escapechar != NO_ESCAPE_CHARACTER && nextChar == quotechar) {
                        sb.append(escapechar).append(nextChar);
                    } else if (escapechar != NO_ESCAPE_CHARACTER && nextChar == escapechar) {
                        sb.append(escapechar).append(nextChar);
                    } else {
                        sb.append(nextChar);
                    }
                }
                if (quotechar != NO_QUOTE_CHARACTER)
                    sb.append(quotechar);
            }
            sb.append(lineEnd);
            pw.write(sb.toString());
        }
        public void close() throws IOException {
            pw.flush();
            pw.close();
        }
        public void flush() throws IOException {
            pw.flush();
        }
    }
*/


    }
