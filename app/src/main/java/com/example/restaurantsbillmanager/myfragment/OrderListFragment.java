package com.example.restaurantsbillmanager.myfragment;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.restaurantsbillmanager.R;
//import com.example.restaurantsbillmanager.activity.HomeActivity;
import com.example.restaurantsbillmanager.activity.OrderDetailPageActivity;
//import com.example.restaurantsbillmanager.activity.UpdateOrderActivity;
import com.example.restaurantsbillmanager.adapter.OrderListAdapter;
import com.example.restaurantsbillmanager.database.OrderTable;
import com.example.restaurantsbillmanager.database.RestaurantDatabase;
import com.example.restaurantsbillmanager.myUtils.DeleteConfirmDialogListener;
import com.example.restaurantsbillmanager.myUtils.GlobalMethods;
import com.example.restaurantsbillmanager.model.OrderListModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderListFragment extends Fragment {


    Date todayDate;
    Date toDate;
    Date fromDate;
    SimpleDateFormat simpleDateFormat;
    SimpleDateFormat serverSimpleDateFormat;
    @BindView(R.id.etOrderListFromDate)
    EditText etOrderListFromDate;
    @BindView(R.id.etOrderListToDate)
    EditText etOrderListToDate;
    RestaurantDatabase restaurantDatabase;
    List<OrderListModel> orderListModelList;
    @BindView(R.id.rvOrderList)
    RecyclerView rvOrderList;
    OrderListAdapter orderListAdapter;
    String TAG = "OrderListFragment";
    public OrderListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_order_list, container, false);
        ButterKnife.bind(this, rootView);
        initAllControls();
        return rootView;
    }

    public void initAllControls()
    {
        todayDate=new Date();
        toDate=todayDate;
        fromDate=todayDate;
        orderListModelList = new ArrayList<>();
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        serverSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        etOrderListFromDate.setText(simpleDateFormat.format(fromDate));
        etOrderListToDate.setText(simpleDateFormat.format(toDate));
        restaurantDatabase = Room.databaseBuilder(getContext(),
                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
        orderListAdapter=new OrderListAdapter(getActivity(), orderListModelList, new OrderListAdapter.OnOrderItemChange() {
//            @Override
//            public void onEditClicked(int position) {
//                editActivityStart(orderListModelList.get(position));
//
//                // Toast.makeText(getContext(),"edit button clicked",Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onDeleteClicked(int position) {
//                onDeleteButtonClicked(position);
//            }
//
//            @Override
//            public void onPrintClicked(int position) {
//                Toast.makeText(getContext(),"print clicked",Toast.LENGTH_SHORT).show();
//            }

            @Override
            public void onItemClicked(int position) {
                Intent intent = new Intent(getActivity(), OrderDetailPageActivity.class);
                intent.putExtra("data",orderListModelList.get(position));
                startActivityForResult(intent,208);
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvOrderList.setLayoutManager(mLayoutManager);
        rvOrderList.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration itemDecor = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecor.setDrawable(getResources().getDrawable(R.drawable.line_divider));
        rvOrderList.addItemDecoration(itemDecor);
        rvOrderList.setAdapter(orderListAdapter);
//        String from=serverSimpleDateFormat.format(fromDate);
//        String to=serverSimpleDateFormat.format(toDate);
     //   getDataFromDatabase(from,to);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== Activity.RESULT_OK);
        {
            orderListModelList.clear();
            String from=serverSimpleDateFormat.format(fromDate);
            String to=serverSimpleDateFormat.format(toDate);
            getDataFromDatabase(from,to);
        }
    }

//    public void onDeleteButtonClicked(int position)
//    {
//        GlobalMethods.showDeleteConfirmDialog(getContext(), new DeleteConfirmDialogListener() {
//            @Override
//            public void onDialogOkButtonClicked() {
//                deleteDataFromLocalDatabase(position);
//            }
//
//            @Override
//            public void onDialogCancelButtonClicked() {
//
//            }
//        });
//    }
//
//    public void deleteDataFromLocalDatabase(int position)
//    {
//        new AsyncTask<Void, Void, Boolean>() {
//            @Override
//            protected Boolean doInBackground(Void... voids) {
//                int delete=  restaurantDatabase.getOrderTableDao().deleteOrderByOrderId(orderListModelList.get(position).getOrderTable().getOrder_id());
//                if(delete>0)
//                {
//                    int data=restaurantDatabase.getOrderItemTableDao().deleteOrderItemByOrderId(orderListModelList.get(position).getOrderTable().getOrder_id());
//                    return true;
//                }
//                else
//                {
//                    return false;
//                }
//            }
//
//            @Override
//            protected void onPostExecute(Boolean aBoolean) {
//                super.onPostExecute(aBoolean);
//                if(aBoolean)
//                {
//                    Toast.makeText(getContext(), "Successfully Delete", Toast.LENGTH_SHORT).show();
//                    String from=serverSimpleDateFormat.format(fromDate);
//                    String to=serverSimpleDateFormat.format(toDate);
//                    getDataFromDatabase(from,to);
//                }
//            }
//        }.execute();
//    }
//
//    public void editActivityStart(OrderListModel orderListModel)
//    {
//        Log.d(TAG, "editActivityStart: "+orderListModel.getOrderTable());
//        Intent intent = new Intent(getContext(), UpdateOrderActivity.class);
//        intent.putExtra("data", orderListModel);
//        startActivity(intent);
//
//    }
//    @Override
//    public void onResume() {
//        super.onResume();
//        if(HomeActivity.getInstance()!=null && HomeActivity.getInstance().tvMainTitle!=null)
//        {
//            HomeActivity.getInstance().tvMainTitle.setText("Order List");
//        }
//    }

    @OnClick(R.id.etOrderListToDate)
    public void onToDateClicked()
    {

        Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String stringDate = dayOfMonth + "-" + (month + 1) + "-" + year;

                try {
                    toDate = simpleDateFormat.parse(stringDate);
                    etOrderListToDate.setText(simpleDateFormat.format(toDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();

    }

    @OnClick(R.id.etOrderListFromDate)
    public void onFromDateClicked()
    {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String stringDate = dayOfMonth + "-" + (month + 1) + "-" + year;

                try {
                    fromDate = simpleDateFormat.parse(stringDate);
                    etOrderListFromDate.setText(simpleDateFormat.format(fromDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }

    @OnClick(R.id.btnOrderListGo)
    public void onGoButtonClicked()
    {
        String from=serverSimpleDateFormat.format(fromDate);
        String to=serverSimpleDateFormat.format(toDate);
        getDataFromDatabase(from,to);
    }

    public void getDataFromDatabase(String from,String to)
    {
        new AsyncTask<Void, Void, List<OrderListModel>>() {
            @Override
            protected List<OrderListModel> doInBackground(Void... voids) {
                OrderListFragment.this.orderListModelList.clear();
                List<OrderTable> orderTableList= restaurantDatabase.getOrderTableDao().fetchtoDateOrFromDateOrderTableData("finish",from, to);
                List<OrderListModel> orderListModelList = new ArrayList<>();
                for(OrderTable order:orderTableList)
                {
                    OrderListModel orderListModel=new OrderListModel();
                    orderListModel.setOrderTable(order);
                    orderListModel.setOrderItemTableList(restaurantDatabase.getOrderItemTableDao().fetchOrderIdWiseSubOrderData(order.getOrder_id()));
                    orderListModelList.add(orderListModel);
                }

                return orderListModelList;
            }

            @Override
            protected void onPostExecute(List<OrderListModel> orderListModelList) {
                super.onPostExecute(orderListModelList);
                if(orderListModelList.size()>0)
                {
                    OrderListFragment.this.orderListModelList.clear();
                    OrderListFragment.this.orderListModelList.addAll(orderListModelList);
                    orderListAdapter.notifyDataSetChanged();
                }
                else
                {
                    Toast.makeText(getContext(), "No Data Found", Toast.LENGTH_SHORT).show();
                    orderListAdapter.notifyDataSetChanged();
                }
            }
        }.execute();
    }

}
