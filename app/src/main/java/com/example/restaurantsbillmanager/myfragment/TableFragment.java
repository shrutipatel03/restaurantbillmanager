package com.example.restaurantsbillmanager.myfragment;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.restaurantsbillmanager.R;
//import com.example.restaurantsbillmanager.activity.AddItemActivity;
//import com.example.restaurantsbillmanager.activity.AddTableActivity;
//import com.example.restaurantsbillmanager.activity.HomeActivity;
import com.example.restaurantsbillmanager.adapter.ItemListAdapter;
import com.example.restaurantsbillmanager.adapter.TableListAdapter;
import com.example.restaurantsbillmanager.database.ItemTable;
import com.example.restaurantsbillmanager.database.RestaurantDatabase;
import com.example.restaurantsbillmanager.database.TableTable;
import com.example.restaurantsbillmanager.myUtils.DeleteConfirmDialogListener;
import com.example.restaurantsbillmanager.myUtils.GlobalMethods;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TableFragment extends Fragment {

    RestaurantDatabase restaurantDatabase;
    String TAG = "TableFragment";
    @BindView(R.id.rvTableList)
    RecyclerView rvTableList;
    @BindView(R.id.etSearchTable)
    EditText etSearchTable;
    List<TableTable> tableTableList;
    TableListAdapter tableListAdapter;
    @BindView(R.id.etAddTableName)
    EditText etAddTableName;
    @BindView(R.id.btnAddTableName)
    Button btnAddTableName;
    TableTable tableTable;
    @BindView(R.id.tvAddTable)
    TextView tvAddTable;
    @BindView(R.id.lladdTable)
    RelativeLayout llAddTable;
    @BindView(R.id.ivclose)
    ImageView ivClose;
    BottomSheetBehavior<RelativeLayout> behavior;
    public TableFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_table, container, false);
        ButterKnife.bind(this,rootView);
        initAllControls();
        return rootView;
    }

    private void initAllControls() {
        tableTableList = new ArrayList<>();
        behavior = BottomSheetBehavior.from(llAddTable);
        behavior.setPeekHeight(150);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        if (getIntent().hasExtra("data")){
//            getSupportActionBar().setTitle("Update Table Name");
//            tableTable = getIntent().getParcelableExtra("data");
//
//
//        }else {
//            getSupportActionBar().setTitle("Add Table Name");
//            btnAddTableName.setText("Add");
//        }
        tableListAdapter = new TableListAdapter(getContext(), tableTableList, new TableListAdapter.OnTableEditOrDeleteListener() {
            @Override
            public void onEditButtonClicked(int position) {
//                Intent intent = new Intent(getContext(), AddTableActivity.class);
//                intent.putExtra("data", tableTableList.get(position));
//                startActivityForResult(intent,208);
                tableTable = tableTableList.get(position);
                etAddTableName.setText(tableTable.getTable_name());
                tvAddTable.setText("Update Table Name");
                btnAddTableName.setText("Update");
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                ivClose.setVisibility(View.VISIBLE);
            }

            @Override
            public void onDeleteButtonClicked(int position) {
                GlobalMethods.showDeleteConfirmDialog(getContext(), new DeleteConfirmDialogListener() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        deleteDataFromLocalDatabase(position);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });
            }
        });
        etSearchTable.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tableListAdapter.getFilter().filter(editable.toString());
            }
        });
        restaurantDatabase = Room.databaseBuilder(getContext(),
                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvTableList.setLayoutManager(mLayoutManager);
        rvTableList.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration itemDecor = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecor.setDrawable(getResources().getDrawable(R.drawable.line_divider));
        rvTableList.addItemDecoration(itemDecor);
        rvTableList.setAdapter(tableListAdapter);
        getDataFromApi();
    }

   /* @OnClick(R.id.fabAddTable)
    public void onfabAddTable(View view){
        Intent intent = new Intent(getContext(), AddTableActivity.class);
        startActivityForResult(intent,208);
    }*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==208 && resultCode== Activity.RESULT_OK)
        {
            getDataFromApi();
        }
    }
    @OnClick(R.id.ivclose)
    public void onClose(){
//        behavior.setPeekHeight(150);
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        onClearData();
    }

    public void onClearData(){
        tvAddTable.setText("Add Table Name");
        btnAddTableName.setText("Add");
        etAddTableName.setText("");
        ivClose.setVisibility(View.INVISIBLE);
//        imgSlideup.setRotation(0);
    }
    public void deleteDataFromLocalDatabase(int position)
    {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                int delete=restaurantDatabase.getTableDao().deleteTableData(tableTableList.get(position));
                if(delete>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if(aBoolean)
                {
                    Toast.makeText(getContext(), "Successfully Delete Item", Toast.LENGTH_SHORT).show();
                    getDataFromApi();
                }
                else
                {
                    Toast.makeText(getContext(), "Failed Delete Item", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    public void getDataFromApi() {
        new AsyncTask<Void, Void, List<TableTable>>() {
            @Override
            protected List<TableTable> doInBackground(Void... voids) {
                return restaurantDatabase.getTableDao().fetchAllTableData();
            }

            @Override
            protected void onPostExecute(List<TableTable> tableTables) {
                super.onPostExecute(tableTables);
                Log.d(TAG, "onPostExecute: " + tableTables.size());
                tableTableList.clear();
                tableTableList.addAll(tableTables);
                tableListAdapter.notifyDataSetChanged();
            }
        }.execute();
    }
    @OnClick(R.id.btnAddTableName)
    public void onItemButtonClicked(View view) {
        String sAddTableName=etAddTableName.getText().toString();
        if(sAddTableName.trim().length()==0){
            Snackbar.make(etAddTableName,"Please enter table name",Snackbar.LENGTH_LONG).show();
        }
        else
        {
            if (tvAddTable.getText().toString().equals("Update Table Name")){
                updateTableName(sAddTableName);
            }else {
                saveTableName(sAddTableName);
            }
        }
    }

    private void updateTableName(String AddTableName) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                tableTable.setTable_name(AddTableName);
                int data = restaurantDatabase.getTableDao().updateTableData(tableTable);
                if (data>0){
                    return true;
                }else{
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean aLong) {
                super.onPostExecute(aLong);
                if(aLong)
                {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    onClearData();
                    Toast.makeText(getContext(), "Update Successfully", Toast.LENGTH_SHORT).show();
                    getDataFromApi();
                }
                else
                {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    onClearData();
                    Toast.makeText(getContext(), "Update Failed", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    public void saveTableName(String AddTableName)
    {
        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {

                TableTable table = new TableTable(AddTableName);
                return restaurantDatabase.getTableDao().insertTableTable(table);
            }

            @Override
            protected void onPostExecute(Long aLong) {
                super.onPostExecute(aLong);
                if(aLong>0)
                {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    onClearData();
                    Toast.makeText(getContext(), "Insert Successfully", Toast.LENGTH_SHORT).show();
                   getDataFromApi();
                }
                else
                {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    onClearData();
                    Toast.makeText(getContext(), "Insert Failed", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }
}
