package com.example.restaurantsbillmanager.myfragment;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.room.Room;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.restaurantsbillmanager.R;
//import com.example.restaurantsbillmanager.activity.HomeActivity;
//import com.example.restaurantsbillmanager.activity.UpdateOrderActivity;
import com.example.restaurantsbillmanager.adapter.ItemSpinnerAdapter;
import com.example.restaurantsbillmanager.adapter.TableNameSpinnerAdapter;
import com.example.restaurantsbillmanager.database.ItemTable;
import com.example.restaurantsbillmanager.database.OrderItemTable;
import com.example.restaurantsbillmanager.database.OrderTable;
import com.example.restaurantsbillmanager.database.RestaurantDatabase;
import com.example.restaurantsbillmanager.database.TableTable;
import com.example.restaurantsbillmanager.model.SaveOrderModel;
import com.example.restaurantsbillmanager.myUtils.DeleteConfirmDialogListener;
import com.example.restaurantsbillmanager.myUtils.GlobalMethods;
import com.google.android.material.snackbar.Snackbar;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderFragment extends Fragment implements DatePickerDialog.OnDateSetListener {


    SimpleDateFormat simpleDateFormat;
    SimpleDateFormat serverSimpleDateFormat;
    Date today;
    Date selectedDate;
    List<View> viewList;
    @BindView(R.id.etCustomerName)
    EditText etCustomerName;
    @BindView(R.id.etCustomerMobile)
    EditText etCustomerMobile;
    //    @BindView(R.id.etTableNumber)
//    EditText etTableNumber;
//    @BindView(R.id.llTableNameContainer)
//    LinearLayout llTableNameContainer;
    @BindView(R.id.spPartName)
    SearchableSpinner spPartName;
    @BindView(R.id.etDate)
    EditText etDate;
    @BindView(R.id.llOrderItemContainer)
    LinearLayout llOrderItemContainer;
    @BindView(R.id.tvTotalBillAmout)
    TextView tvTotalBillAmout;
    ItemSpinnerAdapter itemSpinnerAdapter;
    TableNameSpinnerAdapter tableSpinnerAdapter;
    List<ItemTable> itemTableList;
    List<TableTable> tableTableList;
    RestaurantDatabase restaurantDatabase;
    String TAG = "OrderFragment";

    public OrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_order, container, false);
        ButterKnife.bind(this, rootView);
        initAllControls();
        return rootView;
    }

    public void initAllControls() {
        restaurantDatabase = Room.databaseBuilder(getContext(),
                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
        today = new Date();
        itemTableList = new ArrayList<>();
        tableTableList = new ArrayList<>();
        itemSpinnerAdapter = new ItemSpinnerAdapter(getActivity(), R.layout.spinner_item_inflater, R.id.tvSpinnerItem, itemTableList);
        tableSpinnerAdapter = new TableNameSpinnerAdapter(getActivity(), R.layout.spinner_table_name_inflater, R.id.tvSpinnerTableName, tableTableList);
        selectedDate = today;
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        serverSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        etDate.setText(simpleDateFormat.format(today));
        viewList = new ArrayList<>();
        getItemDataFromLocalDatabase();
        getTableDataFromLocalDatabase();
    }

    @OnClick(R.id.etDate)
    public void onDateClicked() {

        Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(getContext(), this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();

    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        String stringDate = day + "-" + (month + 1) + "-" + year;

        try {
            selectedDate = simpleDateFormat.parse(stringDate);
            etDate.setText(simpleDateFormat.format(selectedDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btnSaveOrder)
    public void onSaaveOrderButtonClicked() {
        String customer_name = etCustomerName.getText().toString();
        String mobile_number = etCustomerMobile.getText().toString();
        String table_number = spPartName.getSelectedItem().toString();
        String date = serverSimpleDateFormat.format(selectedDate);//etDate.getText().toString();

       /* if (!customer_name.isEmpty()) {
//            if (!GlobalMethods.validPersonName(customer_name)) {
                Snackbar.make(etCustomerName, "Please enter valid customer name", Snackbar.LENGTH_LONG).show();

//            }
        }*/
//        else if (!mobile_number.isEmpty()) {
//            if (!mobile_number.matches(GlobalMethods.mobilePattern)) {
//                Snackbar.make(etCustomerMobile, "Please enter valid mobile number", Snackbar.LENGTH_LONG).show();
//
//            }
//        } else */
      /*if (spPartName.getSelectedItemPosition()==0){
            Snackbar.make(spPartName,"Please Select Table Name",Snackbar.LENGTH_LONG).show();
        }*/
        Double total_bill_amount;
        try {
            total_bill_amount = Double.parseDouble(tvTotalBillAmout.getText().toString());
        } catch (NumberFormatException ex) {
            Toast.makeText(getContext(), "Total Amount Not Found", Toast.LENGTH_SHORT).show();
            return;
        }

        List<SaveOrderModel.OrderItem> saveOrderModelListItemList = new ArrayList<>();


        for (View view : viewList) {
            Spinner spItemName = view.findViewById(R.id.spPartName);
            EditText etAddQty = view.findViewById(R.id.etAddQty);
            TextView tvTotalAmount = view.findViewById(R.id.tvTotalAmount);
            if (spItemName.getSelectedItemPosition() == 0) {
                Snackbar.make(spItemName, "Please Select Item Name", Snackbar.LENGTH_LONG).show();
                return;
            }
            if (etAddQty.getText().toString().trim().length() == 0) {
                Snackbar.make(etAddQty, "Please Enter Quantity", Snackbar.LENGTH_LONG).show();
                return;
            }
            SaveOrderModel.OrderItem orderItem = new SaveOrderModel.OrderItem(itemTableList.get(spItemName.getSelectedItemPosition()), Double.parseDouble(etAddQty.getText().toString()), Double.parseDouble(tvTotalAmount.getText().toString()));
            saveOrderModelListItemList.add(orderItem);
        }

        if (spPartName.getSelectedItemPosition() == 0) {
            Snackbar.make(spPartName, "Please Select Table Name", Snackbar.LENGTH_LONG).show();
        }
        else if(!customer_name.isEmpty()&&!GlobalMethods.validPersonName(customer_name)){
            Snackbar.make(etCustomerName, "Please enter valid customer name", Snackbar.LENGTH_LONG).show();

        }
        else if(!mobile_number.isEmpty()&&!mobile_number.matches(GlobalMethods.mobilePattern)){
            Snackbar.make(etCustomerMobile, "Please enter valid mobile number", Snackbar.LENGTH_LONG).show();

        }else {
            SaveOrderModel saveOrderModel = new SaveOrderModel(customer_name, mobile_number, table_number, date, saveOrderModelListItemList, total_bill_amount);
            saveDataToDatabase(saveOrderModel);
        }


    }

    public void saveDataToDatabase(SaveOrderModel saveOrderModel) {
        String status = "active";

        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                OrderTable orderTable = new OrderTable(saveOrderModel.getCustomer_name(), saveOrderModel.getCustomer_mobile(), saveOrderModel.getTable_number(), saveOrderModel.getDate(), saveOrderModel.getOrder_amount(), status);

                long order_id = restaurantDatabase.getOrderTableDao().insertOrderTable(orderTable);
                List<OrderItemTable> orderItemTableList = new ArrayList<>();
                Log.d(TAG, "doInBackground: order_id:" + order_id);
                if (order_id > 0)
                    for (SaveOrderModel.OrderItem data : saveOrderModel.getOrderItemList()) {
                        OrderItemTable orderItemTable = new OrderItemTable(data.getItemTable().getItem_name(), data.getQty(), data.getItemTable().getItem_price(), data.getTotal_amount(), data.getItemTable().getItem_id(), (int) order_id);
                        orderItemTableList.add(orderItemTable);
                    }
                OrderItemTable[] orderItemTables = new OrderItemTable[orderItemTableList.size()];
                orderItemTables = orderItemTableList.toArray(orderItemTables);
                long inserted_record[] = restaurantDatabase.getOrderItemTableDao().bulkInsertOrderItem(orderItemTables);
                Log.d(TAG, "doInBackground: inserted inner" + inserted_record);
                if (inserted_record.length > 0)
                    return true;
                else
                    return false;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean) {
                    Log.d(TAG, "name" + saveOrderModel);
                    Toast.makeText(getContext(), "Successfully Inserted", Toast.LENGTH_SHORT).show();

                    restartFragment();

//                    HomeActivity.getInstance().inflateFragment(2);

                } else {
                    Toast.makeText(getContext(), "failed Inserted", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    private void restartFragment() {

        NavHostFragment navHostFragment = (NavHostFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        Fragment fragment = navHostFragment.getChildFragmentManager().getFragments().get(0);

        if (fragment instanceof OrderFragment) {
            // add your code here
            getFragmentManager().beginTransaction()
                    .detach(fragment)
                    .attach(fragment)
                    .commit();

        }

        etCustomerName.setText("");
        etCustomerMobile.setText("");
        today = new Date();
        etDate.setText(simpleDateFormat.format(today));
    }

    public void setTotalBillNumber() {
        double total_bill_amount = 0;
        for (View view : viewList) {
            TextView tvTotal = view.findViewById(R.id.tvTotalAmount);
            if (tvTotal.getText().toString().trim().length() > 0) {
                total_bill_amount += Double.parseDouble(tvTotal.getText().toString());
            }

        }
        tvTotalBillAmout.setText("" + total_bill_amount);
    }

    @OnClick(R.id.tvAddMoreItem)
    public void onAddMoreItemClicked() {
        addItemData();
    }

    public void getItemDataFromLocalDatabase() {
        new AsyncTask<Void, Void, List<ItemTable>>() {
            @Override
            protected List<ItemTable> doInBackground(Void... voids) {
                return restaurantDatabase.getItemTableDao().fetchAllItemTableData();
            }

            @Override
            protected void onPostExecute(List<ItemTable> itemTableList) {
                super.onPostExecute(itemTableList);
                OrderFragment.this.itemTableList.clear();
                ItemTable itemTable = new ItemTable("Select Item", 0);
                OrderFragment.this.itemTableList.add(itemTable);
                OrderFragment.this.itemTableList.addAll(itemTableList);
                itemSpinnerAdapter.notifyDataSetChanged();
                addItemData();
            }
        }.execute();
    }

    public void getTableDataFromLocalDatabase() {
        new AsyncTask<Void, Void, List<TableTable>>() {
            @Override
            protected List<TableTable> doInBackground(Void... voids) {
                return restaurantDatabase.getTableDao().fetchAllTableData();
            }

            @Override
            protected void onPostExecute(List<TableTable> tableTableList) {
                super.onPostExecute(tableTableList);
                OrderFragment.this.tableTableList.clear();
                TableTable tableTable = new TableTable("Select Table");
                OrderFragment.this.tableTableList.add(tableTable);
                OrderFragment.this.tableTableList.addAll(tableTableList);
                spPartName.setTitle("Select Table");
                spPartName.setPositiveButton("OK");
                spPartName.setAdapter(tableSpinnerAdapter);
                tableSpinnerAdapter.notifyDataSetChanged();
            }


        }.execute();
    }

    public void addItemData() {
        final View child = getLayoutInflater().inflate(R.layout.order_item_layout, null);
        SearchableSpinner spItemName = child.findViewById(R.id.spPartName);
        spItemName.setTitle("Select Item");
        spItemName.setPositiveButton("OK");
        EditText etAddQty = child.findViewById(R.id.etAddQty);
        TextView tvTotalAmount = child.findViewById(R.id.tvTotalAmount);
        etAddQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (spItemName.getSelectedItemPosition() == 0) {

                } else {
                    if (s.toString().trim().length() > 0) {
                        double price = itemTableList.get(spItemName.getSelectedItemPosition()).getItem_price();
                        double qty = Double.parseDouble(s.toString());
                        double total_price = qty * price;
                        tvTotalAmount.setText("" + total_price);

                    } else {
                        tvTotalAmount.setText("");
                    }
                    setTotalBillNumber();

                }
            }
        });
        spItemName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    if (etAddQty.getText().toString().trim().length() > 0) {
                        double price = itemTableList.get(position).getItem_price();
                        double qty = Double.parseDouble(etAddQty.getText().toString());
                        double total_price = qty * price;
                        tvTotalAmount.setText("" + total_price);
                        setTotalBillNumber();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ImageView ivRemoveData = (ImageView) child.findViewById(R.id.ivRemovePart);
        ivRemoveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llOrderItemContainer.removeViewAt(viewList.indexOf(child));
                viewList.remove(viewList.indexOf(child));
                setTotalBillNumber();
            }
        });
        if (viewList.size() == 0) {
            ivRemoveData.setVisibility(View.INVISIBLE);
        } else {
            ivRemoveData.setVisibility(View.VISIBLE);
        }
        spItemName.setAdapter(itemSpinnerAdapter);
        viewList.add(child);
        llOrderItemContainer.addView(child);
    }


//    @Override
//    public void onResume() {
//        super.onResume();
//        if(HomeActivity.getInstance()!=null && HomeActivity.getInstance().tvMainTitle!=null)
//        {
//            HomeActivity.getInstance().tvMainTitle.setText("Order");
//        }
//    }

}
