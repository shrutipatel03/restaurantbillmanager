package com.example.restaurantsbillmanager.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.restaurantsbillmanager.MainActivity;
import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.database.MySharedPreference;
import com.example.restaurantsbillmanager.database.RegisterTable;
import com.example.restaurantsbillmanager.database.RestaurantDatabase;
//import com.example.restaurantsbillmanager.garbege.fragment.LoginFragment;
import com.example.restaurantsbillmanager.myUtils.GlobalMethods;
import com.google.android.material.snackbar.Snackbar;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.etLoginEmail)
    EditText etLoginEmail;
    @BindView(R.id.etLoginPassword)
    EditText etLoginPassword;
    MySharedPreference mySharedPreference;
    RestaurantDatabase restaurantDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initAllControls();
    }

    public void initAllControls()
    {
//        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.login_activity, new LoginFragment(), "login_fragment");
//        fragmentTransaction.commit();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Login");
        getSupportActionBar().hide();
        mySharedPreference = new MySharedPreference(this);
        if (mySharedPreference.getUserLogin()){
            Intent intent = new Intent(LoginActivity.this,SplashScreenActivity.class);
            startActivity(intent);
        }
        restaurantDatabase = Room.databaseBuilder(getApplicationContext(),
                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }
    @OnClick(R.id.btnLogin)
    public void onLoginButtonClicked() {
        String sLoginEmail = etLoginEmail.getText().toString();
        String sLoginPassword = etLoginPassword.getText().toString();
        if (sLoginEmail.isEmpty()){
            Snackbar.make(etLoginEmail,"enter your email-id",Snackbar.LENGTH_LONG).show();
            return;
        }else if (!GlobalMethods.isValidEmail(sLoginEmail)){
            Snackbar.make(etLoginEmail,"please enter your valid email-id",Snackbar.LENGTH_LONG).show();
            return;
        }else if (sLoginPassword.isEmpty()){
            Snackbar.make(etLoginPassword,"enter your Password",Snackbar.LENGTH_LONG).show();
            return;
        }else if(sLoginPassword.length()<6){
            Snackbar.make(etLoginPassword,"password length should be 6 character long",Snackbar.LENGTH_LONG).show();
            return;
        }else {
            insertLoginData(sLoginEmail,sLoginPassword);
        }

    }

    private void insertLoginData(String sLoginEmail, String sLoginPassword) {
        new AsyncTask<Void, Void, RegisterTable>() {
            @Override
            protected RegisterTable doInBackground(Void... voids) {

                RegisterTable registerTable = restaurantDatabase.getRegisterDao().userValidation(sLoginEmail,sLoginPassword);
                return  registerTable;
            }

            @Override
            protected void onPostExecute(RegisterTable aLong) {
                super.onPostExecute(aLong);
                if(aLong != null)
                {
                    String RestaurantName = aLong.getRestaurantName();
                    String RestaurantOwnerName = aLong.getRestaurantOwnerName();
                    String email = aLong.getEmail();
                    String mobile = aLong.getMobile();
                    String password = aLong.getPassword();

                    mySharedPreference.setUserData(RestaurantName,email,RestaurantOwnerName,password,mobile);

                    Toast.makeText(getApplicationContext(), "Insert Successfully", Toast.LENGTH_SHORT).show();
                    Log.d("RegisterFragment","mobile number "+email);
                    Intent intent = new Intent(getApplicationContext(), SplashScreenActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Insert Failed", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    @OnClick(R.id.tvLoginRegister)
    public void onRegisterButtonClicked()
    {
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


}
