package com.example.restaurantsbillmanager.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.adapter.ItemSpinnerAdapter;
import com.example.restaurantsbillmanager.adapter.TableNameSpinnerAdapter;
import com.example.restaurantsbillmanager.database.ItemTable;
import com.example.restaurantsbillmanager.database.OrderItemTable;
import com.example.restaurantsbillmanager.database.OrderTable;
import com.example.restaurantsbillmanager.database.RestaurantDatabase;
import com.example.restaurantsbillmanager.database.TableTable;
//import com.example.restaurantsbillmanager.garbege.fragment.UpdateOrderFragment;
import com.example.restaurantsbillmanager.model.OrderListModel;
import com.example.restaurantsbillmanager.model.SaveOrderModel;
import com.example.restaurantsbillmanager.myUtils.GlobalMethods;
import com.example.restaurantsbillmanager.myfragment.OrderFragment;
import com.google.android.material.snackbar.Snackbar;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateRunningOrderActivity extends AppCompatActivity {

    OrderTable orderTable;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;
    SimpleDateFormat serverSimpleDateFormat;
    Date today;
    Date selectedDate;
    List<View> viewList;
    @BindView(R.id.etCustomerName)
    EditText etCustomerName;
    @BindView(R.id.etCustomerMobile)
    EditText etCustomerMobile;
    @BindView(R.id.spPartName)
    SearchableSpinner spPartName;
    @BindView(R.id.etDate)
    EditText etDate;
    @BindView(R.id.llOrderItemContainer)
    LinearLayout llOrderItemContainer;
    @BindView(R.id.tvTotalBillAmout)
    TextView tvTotalBillAmout;
    ItemSpinnerAdapter itemSpinnerAdapter;
    TableNameSpinnerAdapter tableSpinnerAdapter;
    List<ItemTable> itemTableList;
    List<TableTable> tableTableList;
    OrderListModel orderListModels;
    RestaurantDatabase restaurantDatabase;
    String TAG = "OrderFragment";
    private String tableName;
    DatePickerDialog.OnDateSetListener dateSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_running_order);
        ButterKnife.bind(this);
        initAllControls();
    }

    private void initAllControls() {
        OrderListModel orderListModel1 = getIntent().getParcelableExtra("data");
        OrderListModel orderListModel2 = getIntent().getParcelableExtra("restart data");
        if (getIntent().hasExtra("data")) {
            orderListModels = orderListModel1;
        } else if (getIntent().hasExtra("restart data")) {
            orderListModels = orderListModel2;
            String status = "active";
            orderListModels.getOrderTable().setStatus(status);
        }
        getSupportActionBar().setTitle("View Running Order");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        restaurantDatabase = Room.databaseBuilder(getApplicationContext(),
                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();

        itemTableList = new ArrayList<>();
        tableTableList = new ArrayList<>();
        itemSpinnerAdapter = new ItemSpinnerAdapter(UpdateRunningOrderActivity.this, R.layout.spinner_item_inflater, R.id.tvSpinnerItem, itemTableList);
        tableSpinnerAdapter = new TableNameSpinnerAdapter(UpdateRunningOrderActivity.this, R.layout.spinner_table_name_inflater, R.id.tvSpinnerTableName, tableTableList);
        selectedDate = new Date();
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        serverSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        calendar = Calendar.getInstance();
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLable();
            }

        };
        viewList = new ArrayList<>();
        getItemDataFromLocalDatabase();
        getTableDataFromLocalDatabase();

    }

    private void setpredefinedData() {
        etCustomerName.setText(orderListModels.getOrderTable().getOrder_customer_name());
        etCustomerMobile.setText(orderListModels.getOrderTable().getOrder_customer_mobile());

        try {
            selectedDate = serverSimpleDateFormat.parse(orderListModels.getOrderTable().getOrder_date());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        etDate.setText(simpleDateFormat.format(selectedDate));
        spPartName.setAdapter(tableSpinnerAdapter);
        tvTotalBillAmout.setText("" + orderListModels.getOrderTable().getOrder_total_amount());
        int selection = 0;
        for (int i = 0; i < tableTableList.size(); i++) {
            if (tableTableList.get(i).getTable_name().equals(orderListModels.getOrderTable().getOrder_table_number())) {
                selection = i;
            }
            spPartName.setSelection(selection);
        }
        viewList.clear();
        llOrderItemContainer.removeAllViews();

        for (OrderItemTable orderItemTable : orderListModels.getOrderItemTableList()) {

            final View child = getLayoutInflater().inflate(R.layout.order_item_layout, null);
            SearchableSpinner spItemName = child.findViewById(R.id.spPartName);
            spItemName.setTitle("Select Item");
            spItemName.setPositiveButton("OK");
            EditText etAddQty = child.findViewById(R.id.etAddQty);
            TextView tvTotalAmount = child.findViewById(R.id.tvTotalAmount);
            spItemName.setAdapter(itemSpinnerAdapter);
            int item_spinner_counter = 0;
            for (ItemTable itemTable : itemTableList) {
                if (itemTable.getItem_id() == orderItemTable.getItem_id()) {
                    spItemName.setSelection(item_spinner_counter);
                }
                item_spinner_counter++;
            }
            etAddQty.setText("" + orderItemTable.getOrder_item_qty());
            etAddQty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (spItemName.getSelectedItemPosition() == 0) {

                    } else {
                        if (editable.toString().trim().length() > 0) {
                            double price = itemTableList.get(spItemName.getSelectedItemPosition()).getItem_price();
                            double qauntity = Double.parseDouble(editable.toString());
                            double totalAmount = price * qauntity;
                            tvTotalAmount.setText("" + totalAmount);
                        } else {
                            tvTotalAmount.setText("");
                        }
                        setTotalBillNumber();
                    }
                }
            });
            spItemName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    double price = itemTableList.get(spItemName.getSelectedItemPosition()).getItem_price();
                    double qauntity = Double.parseDouble(etAddQty.getText().toString());
                    double totalAmount = price * qauntity;
                    tvTotalAmount.setText("" + totalAmount);
                    setTotalBillNumber();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            ImageView removeData = (ImageView) child.findViewById(R.id.ivRemovePart);
            removeData.setVisibility(View.INVISIBLE);

            viewList.add(child);
            llOrderItemContainer.addView(child);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.etDate)
    public void onDateClicked() {
        new DatePickerDialog(UpdateRunningOrderActivity.this, dateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLable() {
        String format = "dd-MM-yyyy";
        simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        etDate.setText(simpleDateFormat.format(calendar.getTime()));
    }

    @OnClick(R.id.btnUpdateOrder)
    public void onUpdateOrderButtonClicked() {
        String customer_name = etCustomerName.getText().toString();
        String mobile_number = etCustomerMobile.getText().toString();
        String table_number = spPartName.getSelectedItem().toString();
        String date = serverSimpleDateFormat.format(selectedDate);//etDate.getText().toString();

        Double total_bill_amount;
        try {
            total_bill_amount = Double.parseDouble(tvTotalBillAmout.getText().toString());
        } catch (NumberFormatException ex) {
            Toast.makeText(UpdateRunningOrderActivity.this, "Total Amount Not Found", Toast.LENGTH_SHORT).show();
            return;
        }

     /*   if (!customer_name.isEmpty()) {
            if (!GlobalMethods.validPersonName(customer_name)) {
                Snackbar.make(etCustomerMobile, "Please enter valid customer name", Snackbar.LENGTH_LONG).show();
                return;
            }
        } else if (!mobile_number.isEmpty()) {
            if (!mobile_number.matches(GlobalMethods.mobilePattern)) {
                Snackbar.make(etCustomerMobile, "Please enter valid mobile number", Snackbar.LENGTH_LONG).show();
                return;
            }
        } else if (spPartName.getSelectedItemPosition() == 0) {
            Snackbar.make(spPartName, "Please Select Table Name", Snackbar.LENGTH_LONG).show();
            return;
        }*/
        List<SaveOrderModel.OrderItem> orderItemList = new ArrayList<>();
        for (View view : viewList) {
            SearchableSpinner spItemName = view.findViewById(R.id.spPartName);
            EditText etQuantity = view.findViewById(R.id.etAddQty);
            TextView tvtotalAmount = view.findViewById(R.id.tvTotalAmount);
            if (spItemName.getSelectedItemPosition() == 0) {
                Snackbar.make(spPartName, "Please Select Item Name", Snackbar.LENGTH_LONG).show();
                return;
            } else if (etQuantity.getText().toString().isEmpty()) {
                Snackbar.make(etQuantity, "Please enter quantity", Snackbar.LENGTH_LONG).show();
                return;
            }
            SaveOrderModel.OrderItem orderItem = new SaveOrderModel.OrderItem(itemTableList.get(spItemName.getSelectedItemPosition()), Double.parseDouble(etQuantity.getText().toString()), Double.parseDouble(tvtotalAmount.getText().toString()));
            orderItemList.add(orderItem);
        }

      /*  SaveOrderModel saveOrderModel = new SaveOrderModel(customer_name, mobile_number, table_number, date, orderItemList, TotalBillAmount);
        saveUpdateOrderData(saveOrderModel);*/
        if (spPartName.getSelectedItemPosition() == 0) {
            Snackbar.make(spPartName, "Please Select Table Name", Snackbar.LENGTH_LONG).show();
        }
        else if(!customer_name.isEmpty()&&!GlobalMethods.validPersonName(customer_name)){
            Snackbar.make(etCustomerName, "Please enter valid customer name", Snackbar.LENGTH_LONG).show();

        }
        else if(!mobile_number.isEmpty()&&!mobile_number.matches(GlobalMethods.mobilePattern)){
            Snackbar.make(etCustomerMobile, "Please enter valid mobile number", Snackbar.LENGTH_LONG).show();

        }else {
            SaveOrderModel saveOrderModel = new SaveOrderModel(customer_name, mobile_number, table_number, date, orderItemList, total_bill_amount);
            saveUpdateOrderData(saveOrderModel);
        }

    }

    public void saveUpdateOrderData(SaveOrderModel saveOrderModel) {
        String status = "active";
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                OrderTable orderTable = orderListModels.getOrderTable();
                orderTable.setOrder_customer_name(saveOrderModel.getCustomer_name());
                orderTable.setOrder_customer_mobile(saveOrderModel.getCustomer_mobile());
                orderTable.setOrder_table_number(saveOrderModel.getTable_number());
                orderTable.setOrder_date(saveOrderModel.getDate());
                orderTable.setOrder_total_amount(saveOrderModel.getOrder_amount());
                orderTable.setStatus(status);
                long order_id = restaurantDatabase.getOrderTableDao().updateOrderTableData(orderTable);
                List<OrderItemTable> orderItemTableList = new ArrayList<>();
                if (orderTable.getOrder_id() > 0) {
                    restaurantDatabase.getOrderItemTableDao().deleteOrderItemByOrderId(orderTable.getOrder_id());

                    for (SaveOrderModel.OrderItem data : saveOrderModel.getOrderItemList()) {
                        OrderItemTable orderItemTable = new OrderItemTable(data.getItemTable().getItem_name(), data.getQty(), data.getItemTable().getItem_price(), data.getTotal_amount(), data.getItemTable().getItem_id(), orderTable.getOrder_id());
                        orderItemTableList.add(orderItemTable);
                    }
                    OrderItemTable[] orderItemTables = new OrderItemTable[orderItemTableList.size()];
                    orderItemTables = orderItemTableList.toArray(orderItemTables);
                    long inserted_record[] = restaurantDatabase.getOrderItemTableDao().bulkInsertOrderItem(orderItemTables);
                    Log.d(TAG, "doInBackground: inserted inner" + inserted_record);
                    if (inserted_record.length > 0)
                        return true;
                    else
                        return false;
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean) {
                    Toast.makeText(getApplicationContext(), "Order Successfully Updated", Toast.LENGTH_LONG).show();
                    setResult(RESULT_OK);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed to update", Toast.LENGTH_LONG).show();

                }
            }
        }.execute();
    }
    @OnClick(R.id.btnFinishOrder)
    public void onFinishOrderButtonClicked() {
        String customer_name = etCustomerName.getText().toString();
        String mobile_number = etCustomerMobile.getText().toString();
        String table_number = spPartName.getSelectedItem().toString();
        String date = serverSimpleDateFormat.format(selectedDate);//etDate.getText().toString();

        Double total_bill_amount;
        try {
            total_bill_amount = Double.parseDouble(tvTotalBillAmout.getText().toString());
        } catch (NumberFormatException ex) {
            Toast.makeText(UpdateRunningOrderActivity.this, "Total Amount Not Found", Toast.LENGTH_SHORT).show();
            return;
        }
      /*  if (!customer_name.isEmpty()) {
            if (!GlobalMethods.validPersonName(customer_name)) {
                Snackbar.make(etCustomerMobile, "Please enter valid customer name", Snackbar.LENGTH_LONG).show();
                return;
            }
        } else if (!mobile_number.isEmpty()) {
            if (!mobile_number.matches(GlobalMethods.mobilePattern)) {
                Snackbar.make(etCustomerMobile, "Please enter valid mobile number", Snackbar.LENGTH_LONG).show();
                return;
            }
        } else if (spPartName.getSelectedItemPosition() == 0) {
            Snackbar.make(spPartName, "Please Select Table Name", Snackbar.LENGTH_LONG).show();
            return;
        }*/
        List<SaveOrderModel.OrderItem> orderItemList = new ArrayList<>();
        for (View view : viewList) {
            SearchableSpinner spItemName = view.findViewById(R.id.spPartName);
            EditText etQuantity = view.findViewById(R.id.etAddQty);
            TextView tvtotalAmount = view.findViewById(R.id.tvTotalAmount);
            if (spItemName.getSelectedItemPosition() == 0) {
                Snackbar.make(spPartName, "Please Select Item Name", Snackbar.LENGTH_LONG).show();
                return;
            } else if (etQuantity.getText().toString().isEmpty()) {
                Snackbar.make(etQuantity, "Please enter quantity", Snackbar.LENGTH_LONG).show();
                return;
            }
            SaveOrderModel.OrderItem orderItem = new SaveOrderModel.OrderItem(itemTableList.get(spItemName.getSelectedItemPosition()), Double.parseDouble(etQuantity.getText().toString()), Double.parseDouble(tvtotalAmount.getText().toString()));
            orderItemList.add(orderItem);
        }
      /*  double TotalBillAmount = Double.parseDouble(tvTotalBillAmout.getText().toString());
        if (tvTotalBillAmout.getText().toString().isEmpty()) {
            Snackbar.make(tvTotalBillAmout, "Please enter quantity", Snackbar.LENGTH_LONG).show();
            return;
        }*/
       /* SaveOrderModel saveOrderModel = new SaveOrderModel(customer_name, mobile_number, table_number, date, orderItemList, TotalBillAmount);
        saveFinishOrderData(saveOrderModel);*/
        if (spPartName.getSelectedItemPosition() == 0) {
            Snackbar.make(spPartName, "Please Select Table Name", Snackbar.LENGTH_LONG).show();
        }
        else if(!customer_name.isEmpty()&&!GlobalMethods.validPersonName(customer_name)){
            Snackbar.make(etCustomerName, "Please enter valid customer name", Snackbar.LENGTH_LONG).show();

        }
        else if(!mobile_number.isEmpty()&&!mobile_number.matches(GlobalMethods.mobilePattern)){
            Snackbar.make(etCustomerMobile, "Please enter valid mobile number", Snackbar.LENGTH_LONG).show();

        }else {
            SaveOrderModel saveOrderModel = new SaveOrderModel(customer_name, mobile_number, table_number, date, orderItemList, total_bill_amount);
            saveFinishOrderData(saveOrderModel);
        }

    }

    public void saveFinishOrderData(SaveOrderModel saveOrderModel) {
        String status = "finish";
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                OrderTable orderTable = orderListModels.getOrderTable();
                orderTable.setOrder_customer_name(saveOrderModel.getCustomer_name());
                orderTable.setOrder_customer_mobile(saveOrderModel.getCustomer_mobile());
                orderTable.setOrder_table_number(saveOrderModel.getTable_number());
                orderTable.setOrder_date(saveOrderModel.getDate());
                orderTable.setOrder_total_amount(saveOrderModel.getOrder_amount());
                orderTable.setStatus(status);
                long order_id = restaurantDatabase.getOrderTableDao().updateOrderTableData(orderTable);
                List<OrderItemTable> orderItemTableList = new ArrayList<>();
                if (orderTable.getOrder_id() > 0) {
                    restaurantDatabase.getOrderItemTableDao().deleteOrderItemByOrderId(orderTable.getOrder_id());

                    for (SaveOrderModel.OrderItem data : saveOrderModel.getOrderItemList()) {
                        OrderItemTable orderItemTable = new OrderItemTable(data.getItemTable().getItem_name(), data.getQty(), data.getItemTable().getItem_price(), data.getTotal_amount(), data.getItemTable().getItem_id(), orderTable.getOrder_id());
                        orderItemTableList.add(orderItemTable);
                    }
                    OrderItemTable[] orderItemTables = new OrderItemTable[orderItemTableList.size()];
                    orderItemTables = orderItemTableList.toArray(orderItemTables);
                    long inserted_record[] = restaurantDatabase.getOrderItemTableDao().bulkInsertOrderItem(orderItemTables);
                    Log.d(TAG, "doInBackground: inserted inner" + inserted_record);
                    if (inserted_record.length > 0)
                        return true;
                    else
                        return false;
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean) {
                    Toast.makeText(getApplicationContext(), "Order Successfully Finish", Toast.LENGTH_LONG).show();
                    setResult(RESULT_OK);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed to finish", Toast.LENGTH_LONG).show();

                }
            }
        }.execute();
    }

    public void setTotalBillNumber() {

        double total_bill_amount = 0;

        for (View view : viewList) {
            TextView tvTotal = view.findViewById(R.id.tvTotalAmount);
            if (tvTotal.getText().toString().trim().length() > 0) {
                total_bill_amount += Double.parseDouble(tvTotal.getText().toString());
            }

        }
        tvTotalBillAmout.setText("" + total_bill_amount);
    }

    @OnClick(R.id.tvAddMoreItem)
    public void onAddMoreItemClicked() {
        addItemData();
    }

    public void getItemDataFromLocalDatabase() {
        new AsyncTask<Void, Void, List<ItemTable>>() {
            @Override
            protected List<ItemTable> doInBackground(Void... voids) {
                return restaurantDatabase.getItemTableDao().fetchAllItemTableData();
            }

            @Override
            protected void onPostExecute(List<ItemTable> itemTableList) {
                super.onPostExecute(itemTableList);
                UpdateRunningOrderActivity.this.itemTableList.clear();
                ItemTable itemTable = new ItemTable("Select Item", 0);
                UpdateRunningOrderActivity.this.itemTableList.add(itemTable);
                UpdateRunningOrderActivity.this.itemTableList.addAll(itemTableList);

                itemSpinnerAdapter.notifyDataSetChanged();

            }
        }.execute();
    }

    public void getTableDataFromLocalDatabase() {
        new AsyncTask<Void, Void, List<TableTable>>() {
            @Override
            protected List<TableTable> doInBackground(Void... voids) {
                return restaurantDatabase.getTableDao().fetchAllTableData();
            }

            @Override
            protected void onPostExecute(List<TableTable> tableTableList) {
                super.onPostExecute(tableTableList);
                UpdateRunningOrderActivity.this.tableTableList.clear();


                TableTable tableTable = new TableTable("select table");

                UpdateRunningOrderActivity.this.tableTableList.add(tableTable);
                UpdateRunningOrderActivity.this.tableTableList.addAll(tableTableList);
                spPartName.setPositiveButton("OK");
                spPartName.setAdapter(tableSpinnerAdapter);

                tableSpinnerAdapter.notifyDataSetChanged();
                setpredefinedData();

            }


        }.execute();
    }

    public void addItemData() {
        final View child = getLayoutInflater().inflate(R.layout.order_item_layout, null);
        SearchableSpinner spItemName = child.findViewById(R.id.spPartName);
        spItemName.setTitle("Select Item");
        spItemName.setPositiveButton("OK");
        EditText etAddQty = child.findViewById(R.id.etAddQty);
        TextView tvTotalAmount = child.findViewById(R.id.tvTotalAmount);
        etAddQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (spItemName.getSelectedItemPosition() == 0) {

                } else {
                    if (s.toString().trim().length() > 0) {
                        double price = itemTableList.get(spItemName.getSelectedItemPosition()).getItem_price();
                        double qty = Double.parseDouble(s.toString());
                        double total_price = qty * price;
                        tvTotalAmount.setText("" + total_price);

                    } else {
                        tvTotalAmount.setText("");
                    }
                    setTotalBillNumber();

                }
            }
        });
        spItemName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    if (etAddQty.getText().toString().trim().length() > 0) {
                        double price = itemTableList.get(position).getItem_price();
                        double qty = Double.parseDouble(etAddQty.getText().toString());
                        double total_price = qty * price;
                        tvTotalAmount.setText("" + total_price);
                        setTotalBillNumber();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ImageView ivRemoveData = (ImageView) child.findViewById(R.id.ivRemovePart);
        ivRemoveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llOrderItemContainer.removeViewAt(viewList.indexOf(child));
                viewList.remove(viewList.indexOf(child));
                setTotalBillNumber();

            }
        });
        if (viewList.size() == 0) {
            ivRemoveData.setVisibility(View.INVISIBLE);
        } else {
            ivRemoveData.setVisibility(View.VISIBLE);
        }
        spItemName.setAdapter(itemSpinnerAdapter);
        viewList.add(child);
        llOrderItemContainer.addView(child);
    }

}
