//package com.example.restaurantsbillmanager.activity;
//
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.room.Room;
//
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import com.example.restaurantsbillmanager.R;
//import com.example.restaurantsbillmanager.database.RestaurantDatabase;
//import com.example.restaurantsbillmanager.database.TableTable;
//import com.google.android.material.snackbar.Snackbar;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//
//public class AddTableActivity extends AppCompatActivity {
//
//    @BindView(R.id.etAddTableName)
//    EditText etAddTableName;
//    @BindView(R.id.btnAddTableName)
//    Button btnAddTableName;
//    RestaurantDatabase restaurantDatabase;
//    TableTable tableTable;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_table);
//        ButterKnife.bind(this);
//        initAllControls();
//    }
//
//    private void initAllControls() {
//        restaurantDatabase= Room.databaseBuilder(getApplicationContext(),
//                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
//                .fallbackToDestructiveMigration()
//                .build();
//
//
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//        if (getIntent().hasExtra("data")){
//            getSupportActionBar().setTitle("Update Table Name");
//            tableTable = getIntent().getParcelableExtra("data");
//            etAddTableName.setText(tableTable.getTable_name());
//            btnAddTableName.setText("Update");
//
//        }else {
//            getSupportActionBar().setTitle("Add Table Name");
//            btnAddTableName.setText("Add");
//        }
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == android.R.id.home){
//            onBackPressed();
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    @OnClick(R.id.btnAddTableName)
//    public void onItemButtonClicked(View view) {
//        String sAddTableName=etAddTableName.getText().toString();
//
//        if(sAddTableName.trim().length()==0){
//            Snackbar.make(etAddTableName,"Please enter table name",Snackbar.LENGTH_LONG).show();
//        }
//        else
//        {
//            if (getIntent().hasExtra("data")){
//                updateTableName(sAddTableName);
//            }else {
//                saveTableName(sAddTableName);
//            }
//        }
//    }
//
//    private void updateTableName(String AddTableName) {
//        new AsyncTask<Void, Void, Boolean>() {
//            @Override
//            protected Boolean doInBackground(Void... voids) {
//                tableTable.setTable_name(AddTableName);
//                int data = restaurantDatabase.getTableDao().updateTableData(tableTable);
//                if (data>0){
//                    return true;
//                }else{
//                   return false;
//                }
//            }
//
//            @Override
//            protected void onPostExecute(Boolean aLong) {
//                super.onPostExecute(aLong);
//                if(aLong)
//                {
//                    Toast.makeText(getApplicationContext(), "Update Successfully", Toast.LENGTH_SHORT).show();
//                    setResult(RESULT_OK);
//                    finish();
//                }
//                else
//                {
//                    Toast.makeText(getApplicationContext(), "Update Failed", Toast.LENGTH_SHORT).show();
//                }
//            }
//        }.execute();
//    }
//
//    public void saveTableName(String AddTableName)
//    {
//        new AsyncTask<Void, Void, Long>() {
//            @Override
//            protected Long doInBackground(Void... voids) {
//
//                TableTable table = new TableTable(AddTableName);
//                return restaurantDatabase.getTableDao().insertTableTable(table);
//            }
//
//            @Override
//            protected void onPostExecute(Long aLong) {
//                super.onPostExecute(aLong);
//                if(aLong>0)
//                {
//                    Toast.makeText(getApplicationContext(), "Insert Successfully", Toast.LENGTH_SHORT).show();
//                    setResult(RESULT_OK);
//                    finish();
//                }
//                else
//                {
//                    Toast.makeText(getApplicationContext(), "Insert Failed", Toast.LENGTH_SHORT).show();
//                }
//            }
//        }.execute();
//    }
//}
