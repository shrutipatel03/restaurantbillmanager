package com.example.restaurantsbillmanager.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.restaurantsbillmanager.MainActivity;
import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.database.MySharedPreference;

public class SplashScreenActivity extends AppCompatActivity {

    MySharedPreference mySharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        initAllControls();
    }

    public void initAllControls() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().hide();
        mySharedPreference = new MySharedPreference(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                   Intent i=new Intent(SplashScreenActivity.this,MainActivity.class);
                  i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                   startActivity(i);
                   finish();

            }
        },4000);

    }
}
