package com.example.restaurantsbillmanager.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.database.RegisterTable;
import com.example.restaurantsbillmanager.database.RestaurantDatabase;
//import com.example.restaurantsbillmanager.garbege.fragment.RegisterFragment;
import com.example.restaurantsbillmanager.myUtils.GlobalMethods;
import com.google.android.material.snackbar.Snackbar;

import static java.security.AccessController.getContext;

public class RegisterActivity extends AppCompatActivity {
    @BindView(R.id.etRegisterRestuarantName)
    EditText etRegisterRestuarantName;
    @BindView(R.id.etRegisterRestuarantOwnerName)
    EditText etRegisterRestuarantOwnerName;
    @BindView(R.id.etRegisterEmail)
    EditText etRegisterEmail;
    @BindView(R.id.etRegisterMobile)
    EditText etRegisterMobile;
    @BindView(R.id.etRegisterPassword)
    EditText etRegisterPassword;
    @BindView(R.id.etRegisterConfirmPassword)
    EditText etRegisterConfirmPassword;
    RestaurantDatabase restaurantDatabase;
    String message="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initAllControls();
        ButterKnife.bind(this);
    }

    public void initAllControls()
    {
        restaurantDatabase= Room.databaseBuilder(getApplicationContext(),
                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Register");
//        getSupportActionBar().hide();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.tvRegisterLogin)
    public void ontvLoginRegister(){
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.btnRegister)
    public void onbtnRegister(){
        String sRegisterRestuarantName = etRegisterRestuarantName.getText().toString();
        String sRegisterRestuarantOwnerName = etRegisterRestuarantOwnerName.getText().toString();
        String sRegisterEmail = etRegisterEmail.getText().toString();
        String sRegisterMobile = etRegisterMobile.getText().toString();
        String sRegisterPassword = etRegisterPassword.getText().toString();
        String sRegisterConfirmPassword = etRegisterConfirmPassword.getText().toString();

        if (sRegisterRestuarantName.isEmpty()){
            Snackbar.make(etRegisterRestuarantName,"enter your name",Snackbar.LENGTH_LONG).show();
            return;
        }else if (!GlobalMethods.validPersonName(sRegisterRestuarantName)){
            Snackbar.make(etRegisterRestuarantName,"please enter your valid name",Snackbar.LENGTH_LONG).show();
            return;
        }if (sRegisterRestuarantOwnerName.isEmpty()){
            Snackbar.make(etRegisterRestuarantOwnerName,"enter your name",Snackbar.LENGTH_LONG).show();
            return;
        }else if (!GlobalMethods.validPersonName(sRegisterRestuarantOwnerName)){
            Snackbar.make(etRegisterRestuarantOwnerName,"please enter your valid name",Snackbar.LENGTH_LONG).show();
            return;
        }else if (sRegisterEmail.isEmpty()){
            Snackbar.make(etRegisterEmail,"enter your email-id",Snackbar.LENGTH_LONG).show();
            return;
        }else if (!GlobalMethods.isValidEmail(sRegisterEmail)){
            Snackbar.make(etRegisterEmail,"please enter your valid email-id",Snackbar.LENGTH_LONG).show();
            return;
        }else if (sRegisterMobile.isEmpty()){
            Snackbar.make(etRegisterMobile,"enter your mobile number",Snackbar.LENGTH_LONG).show();
            return;
        }else if (!sRegisterMobile.matches(GlobalMethods.mobilePattern)){
            Snackbar.make(etRegisterMobile,"please enter valid mobile number",Snackbar.LENGTH_LONG).show();
            return;
        }else if (sRegisterPassword.isEmpty()){
            Snackbar.make(etRegisterPassword,"enter your Password",Snackbar.LENGTH_LONG).show();
            return;
        }else if (sRegisterConfirmPassword.isEmpty()) {
            Snackbar.make(etRegisterConfirmPassword, "enter confirm password", Snackbar.LENGTH_LONG).show();
            return;
        } else if(!sRegisterConfirmPassword.matches(sRegisterPassword)){
            Snackbar.make(etRegisterConfirmPassword,"confirm password not matched",Snackbar.LENGTH_LONG).show();
            return;
        }else if(sRegisterPassword.length()<6){
            Snackbar.make(etRegisterPassword,"password length should be 6 character long",Snackbar.LENGTH_LONG).show();
            return;
        }else {
            insertRegisterData(sRegisterRestuarantName,sRegisterRestuarantOwnerName,sRegisterEmail,sRegisterMobile,sRegisterPassword,sRegisterConfirmPassword);
        }
    }

    private void insertRegisterData(String RestuarantName,String RestuarantOwnerName,String email,String mobile,String password,String confirmPassword) {
        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                RegisterTable registerTable = new RegisterTable(RestuarantName,RestuarantOwnerName,email,mobile,password,confirmPassword);
                RegisterTable table = restaurantDatabase.getRegisterDao().userExist(mobile,email);
                if (table == null){
                    Long data = restaurantDatabase.getRegisterDao().insertRegisterData(registerTable);
                    return data;
                }else{

                    if (registerTable.getMobile().equals(mobile)) {
//                        Snackbar.make(etRegisterMobile, "Mobile  or email  already Exist please login", Snackbar.LENGTH_LONG).show();
                        message="mobile or email already exiest";
                        return 0l;
                    }else{
//                        Snackbar.make(etRegisterEmail,"Email already Exist",Snackbar.LENGTH_LONG).show();
                        message="email already exiest";
                        return 0l;
                    }
                }
            }



            @Override
            protected void onPostExecute(Long aLong) {
                super.onPostExecute(aLong);
                if(aLong>0) {
                    Toast.makeText(getApplicationContext(), "Insert Successfully", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                    Log.d("RegisterFragment","mobile number "+mobile);
                }
                else if(message.trim().equals(""))
                {
                    Toast.makeText(RegisterActivity.this,"something went wrong",Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }
}
