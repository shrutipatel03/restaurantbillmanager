package com.example.restaurantsbillmanager.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.restaurantsbillmanager.R;
import com.example.restaurantsbillmanager.adapter.ItemSpinnerAdapter;
import com.example.restaurantsbillmanager.database.ItemTable;
import com.example.restaurantsbillmanager.database.MySharedPreference;
import com.example.restaurantsbillmanager.database.OrderItemTable;
import com.example.restaurantsbillmanager.database.RestaurantDatabase;
//import com.example.restaurantsbillmanager.garbege.fragment.OrderDetailFragment;
import com.example.restaurantsbillmanager.model.OrderListModel;
import com.example.restaurantsbillmanager.myUtils.DeleteConfirmDialogListener;
import com.example.restaurantsbillmanager.myUtils.GlobalMethods;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.uttampanchasara.pdfgenerator.CreatePdf;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class OrderDetailPageActivity extends AppCompatActivity {

    OrderListModel orderListModel;
    @BindView(R.id.tvOrderCustomerName)
    TextView tvOrderCustomerName;
    @BindView(R.id.tvOrderTableNo)
    TextView tvOrderTableNo;
    @BindView(R.id.tvOrderCustomerMobileNo)
    TextView tvOrderCustomerMobileNo;
    @BindView(R.id.tvOrderDate)
    TextView tvOrderDate;
    @BindView(R.id.tvOrderTotalAmount)
    TextView tvOrderTotalAmount;
    @BindView(R.id.llOrderItemContainer)
    LinearLayout llOrderItemContainer;
    RestaurantDatabase restaurantDatabase;
    List<View> viewList;
    MySharedPreference mySharedPreference;
    List<OrderItemTable> orderItemTable;
    private SimpleDateFormat simpleDateFormat;
    private SimpleDateFormat serverSimpleDateFormat;
    private MenuItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail_page);
        ButterKnife.bind(this);
        initAllControls();
    }
    public void initAllControls(){
        getSupportActionBar().setTitle("Order Detail Page");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        restaurantDatabase = Room.databaseBuilder(getApplicationContext(),
                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
        viewList = new ArrayList<>();
        orderItemTable = new ArrayList<>();
        mySharedPreference = new MySharedPreference(this);
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        serverSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        orderListModel = getIntent().getParcelableExtra("data");
        orderItemTable = orderListModel.getOrderItemTableList();
        if (orderListModel.getOrderTable().getOrder_customer_name().equals("")){
            tvOrderCustomerName.setText("Not mentioned");
        }else {
            tvOrderCustomerName.setText("" + orderListModel.getOrderTable().getOrder_customer_name());
        }
        if (orderListModel.getOrderTable().getOrder_customer_mobile().equals("")){
            tvOrderCustomerMobileNo.setText("Not mentioned");
        }else {
            tvOrderCustomerMobileNo.setText("" + orderListModel.getOrderTable().getOrder_customer_mobile());
        }
        try {
            tvOrderDate.setText("" + simpleDateFormat.format(serverSimpleDateFormat.parse(orderListModel.getOrderTable().getOrder_date())));

        }catch (ParseException e){
            e.printStackTrace();
        }
        tvOrderTableNo.setText("" + orderListModel.getOrderTable().getOrder_table_number());
        tvOrderTotalAmount.setText("" + orderListModel.getOrderTable().getOrder_total_amount() + "");
        for (OrderItemTable orderItemTable : orderListModel.getOrderItemTableList()) {

            final View child = getLayoutInflater().inflate(R.layout.order_item__list_layout, null);
            TextView tvItemName = (TextView) child.findViewById(R.id.tvItemName);
            tvItemName.setText(orderItemTable.getOrder_item_name());
            TextView tvOrderQty = (TextView) child.findViewById(R.id.tvOrderQty);
            tvOrderQty.setText(orderItemTable.getOrder_item_qty() + "");
            TextView tvTotalAmount = (TextView) child.findViewById(R.id.tvTotalAmount);
            tvTotalAmount.setText("₹"+orderItemTable.getOrder_total_amount());

             viewList.add(child);
            llOrderItemContainer.addView(child);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        else if(item.getItemId()==R.id.download_report){
            if (orderListModel != null) {
                Dexter.withActivity(this)
                        .withPermissions(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        ).withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        /* ... */
                        if (report.areAllPermissionsGranted()) {
                            DownloadPdf();
                            Toast.makeText(getApplicationContext(), "Downloading...", Toast.LENGTH_LONG).show();
                        } else {
                            showPermissionDailog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
            } else {
                Toast.makeText(getApplicationContext(), "no data found", Toast.LENGTH_LONG).show();
            }

        }
        return false;
    }
    @OnClick(R.id.btnEditOrder)
    public void onEditButtonClicked(View view) {
        Intent intent = new Intent(getApplicationContext(), UpdateRunningOrderActivity.class);
        intent.putExtra("restart data", orderListModel);
        startActivityForResult(intent,208);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==208 && resultCode==Activity.RESULT_OK)
        {
            setResult(Activity.RESULT_OK);
            finish();
        }
    }

    @OnClick(R.id.btnDeleteOrder)
    public void onDeleteButtonClicked() {

        GlobalMethods.showDeleteConfirmDialog(OrderDetailPageActivity.this, new DeleteConfirmDialogListener() {
            @Override
            public void onDialogOkButtonClicked() {
                deleteDataFromLocalDatabase();
            }

            @Override
            public void onDialogCancelButtonClicked() {

            }
        });

    }
    public void deleteDataFromLocalDatabase() {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                int delete = restaurantDatabase.getOrderTableDao().deleteOrderByOrderId(orderListModel.getOrderTable().getOrder_id());
                if (delete > 0) {
                    int data = restaurantDatabase.getOrderItemTableDao().deleteOrderItemByOrderId(orderListModel.getOrderTable().getOrder_id());
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean) {
                    setResult(Activity.RESULT_OK);
                    onBackPressed();
                }


            }
        }.execute();
    }

  /*  @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.report_download, menu);
        item = menu.findItem(R.id.download_report);
        item.setVisible(false);

    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.report_download,menu);
        item = menu.findItem(R.id.download_report);
        return true;
    }

    private void DownloadPdf() {
        File directory = createDirectory();

        String content = "<html>" +
                "    <body>" +
                "<style>"+
                "    .invoice-box table tr.heading td {\n" +
                "        background: #F5B041;\n" +
                "        float: left;\n" +
                "       width: 25%;\n" +
                "        border-bottom: 1px solid #ddd;\n" +
                "        font-weight: bold;\n" +
                "    }\n" +
                "</style>"+
                "        <table width='100%' >" +
                "            <tbody>" +
                "                <tr>" +
                "                               <center><img border=\"0\" src=\"file:///android_res/drawable/appicon.png\" alt=\"nothing\" width=\"60\" height=\"60\" />" +
                "                                     <h1>"+mySharedPreference.getRestourantOwerName()+"</h1></center>"+

                "                </tr>" +
                "                <tr>" +
                "                    <td>" +
                "                                Customer Name : "+orderListModel.getOrderTable().getOrder_customer_name()+"<br>\n" +
                "                                Customer Mobile : "+orderListModel.getOrderTable().getOrder_customer_mobile()+"<br>\n" +
                 "                                 Order Date: "+tvOrderDate.getText().toString()+"<br>\n" +
                 "                                 Table Name: "+orderListModel.getOrderTable().getOrder_table_number()+"<br>\n" +
                "                    </td>" +
                "                    <td align='right'>" +
                "                    </td>" +
                "                </tr>" +
                "            </tbody>" +
                "        </table>" +
                "        <hr>" +
                "        <table border='1' cellspacing='0' align='center' width='100%'>" +
                "            <tbody>" +
                "                <tr bgcolor=#F5B041>" +
                "                    <th align='center'>" +"Item Name"+ "</th>" +
                "                    <th align='center'>" + "Item Quantity" + "</th>" +
                "                    <th align='center'>" + "Per Item Price" + "</th>" +
                "                    <th align='center'>" + "Total Amount" + "</th>" +
                "                </tr>";
        for (OrderItemTable orderItemTable : orderListModel.getOrderItemTableList()) {
            content += "                <tr>" +
                    "                    <td align='center'>" + orderItemTable.getOrder_item_name()+ "</td>" +
                    "                    <td align='center'>" + orderItemTable.getOrder_item_qty() + "</td>" +
                    "                    <td align='center'>" + orderItemTable.getOrder_item_price() + "</td>" +
                    "                    <td align='center'>" + orderItemTable.getOrder_total_amount() + "</td>" +

              "                </tr>";
        }

        content += "            </tbody>" +
                "        </table>" +
                "    </body>" +
                "</html>";
        new CreatePdf(getApplicationContext())
                .setPdfName("Order_report_" + mySharedPreference.getRestourantName() + "_" + orderListModel.getOrderTable().getOrder_customer_name()+"_"+ orderListModel.getOrderTable().getOrder_total_amount())
                .openPrintDialog(false)
                .setContentBaseUrl(null)
                .setContent(content)
                .setFilePath(directory.getAbsolutePath() + File.separator)
                .setCallbackListener(new CreatePdf.PdfCallbackListener() {
                    @Override
                    public void onFailure(String s) {
                        Toast.makeText(getApplicationContext(),"failed download", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(String s) {
                        String file_path=directory.getAbsolutePath() + File.separator+"General_report_" + mySharedPreference.getRestourantName() + "_" + orderListModel.getOrderTable().getOrder_customer_name();
                        Toast.makeText(getApplicationContext(), "download complete "+file_path, Toast.LENGTH_SHORT).show();
                    }
                })
                .create();

    }

    private File createDirectory() {
        File mFile;
        if (isExternalStorageWritable()) {
            mFile = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "restaurant_bill_manager");
        } else {
            mFile = new File(Environment.getDataDirectory().toString() + File.separator + "restaurant_bill_manager");
        }

        if (!mFile.exists())
            mFile.mkdirs();
        return mFile;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public void showPermissionDailog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Need Send SMS Permission");
        builder.setMessage("This app needs SMS permission for send please give permission from settings.");
        builder.setPositiveButton("Go To Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
}
