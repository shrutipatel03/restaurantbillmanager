//package com.example.restaurantsbillmanager.activity;
//
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.fragment.app.FragmentTransaction;
//import androidx.room.Room;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//
//import android.app.Activity;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.example.restaurantsbillmanager.R;
//import com.example.restaurantsbillmanager.database.ItemTable;
//import com.example.restaurantsbillmanager.database.RestaurantDatabase;
//import com.example.restaurantsbillmanager.garbege.fragment.AddItemFragment;
//import com.example.restaurantsbillmanager.model.OrderListModel;
//import com.example.restaurantsbillmanager.myfragment.ItemListFragment;
//import com.example.restaurantsbillmanager.myfragment.OrderFragment;
//
//public class AddItemActivity extends AppCompatActivity {
//
//    ItemTable itemTable;
////    @BindView(R.id.tvMainTitle)
////    TextView tvMainTitle;
//    @BindView(R.id.etItemName)
//    EditText etItemName;
//    @BindView(R.id.etItemPrice)
//    EditText etItemPrice;
//    @BindView(R.id.btnSaveItem)
//    Button btnSaveItem;
//    RestaurantDatabase restaurantDatabase;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_add_item);
//        ButterKnife.bind(this);
//        initAllControls();
//    }
//    public void initAllControls()
//    {
//
//        itemTable=(ItemTable) getIntent().getParcelableExtra("data");//.getParcelable("data");//.getParcelableExtra("data");
//        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
//        ItemListFragment updateOrderFragment=new ItemListFragment();
//        Bundle bundle = new Bundle();
//        bundle.putParcelable("data", itemTable);
//        updateOrderFragment.setArguments(bundle);
//        fragmentTransaction.replace(R.id.update_order_container,updateOrderFragment,"update_order");
//        fragmentTransaction.commit();
////        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
////        if(getIntent().hasExtra("data"))
////        {
////            tvMainTitle.setText("Update Item");
////            itemTable=(ItemTable)getIntent().getParcelableExtra("data");
////            UpdateItemFragment updateItemFragment=new UpdateItemFragment();
////            Bundle bundle = new Bundle();
////            bundle.putParcelable("data", itemTable);
////            updateItemFragment.setArguments(bundle);
////            fragmentTransaction.replace(R.id.add_item, updateItemFragment, "add_fragment");
////            fragmentTransaction.commit();
////        }
////        else {
////
////            fragmentTransaction.replace(R.id.add_item, new AddItemFragment(), "add_fragment");
////            fragmentTransaction.commit();
////            tvMainTitle.setText("Add Item");
////        }
//        restaurantDatabase= Room.databaseBuilder(getApplicationContext(),
//                RestaurantDatabase.class, RestaurantDatabase.DATABASE_NAME)
//                .fallbackToDestructiveMigration()
//                .build();
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        if (getIntent().hasExtra("data")){
//            getSupportActionBar().setTitle("Update Item");
//            itemTable = getIntent().getParcelableExtra("data");
//            etItemName.setText(itemTable.getItem_name());
//            etItemPrice.setText(""+itemTable.getItem_price());
//            btnSaveItem.setText("Update");
//        }else {
//            getSupportActionBar().setTitle("Add Item");
//            btnSaveItem.setText("Add");
//        }
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == android.R.id.home){
//            onBackPressed();
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
////    @OnClick(R.id.btnSaveItem)
////    public void onItemButtonClicked(View view) {
////        String itemName=etItemName.getText().toString();
////        String price=etItemPrice.getText().toString();
////        if(itemName.trim().length()==0)
////        {
////            Toast.makeText(getApplicationContext(),"Please Enter Item Name",Toast.LENGTH_SHORT).show();
////        }
////        else if(price.trim().length()==0)
////        {
////            Toast.makeText(getApplicationContext(),"Please Enter Item Price",Toast.LENGTH_SHORT).show();
////        }
////        else
////        {
////            if (getIntent().hasExtra("data")){
////                updateItemName(itemName,price);
////            }else {
////                saveItemName(itemName, price);
////            }
////        }
////    }
////
////    public void saveItemName(String itemName,String itemPrice)
////    {
////        new AsyncTask<Void, Void, Long>() {
////            @Override
////            protected Long doInBackground(Void... voids) {
////
////                ItemTable itemTable;
////                try {
////                    itemTable = new ItemTable(itemName,Double.parseDouble(itemPrice));
////                }catch (NumberFormatException ex)
////                {
////                    itemTable = new ItemTable(itemName,0);
////                }
////
////                return restaurantDatabase.getItemTableDao().insertItemTable(itemTable);
////            }
////
////            @Override
////            protected void onPostExecute(Long aLong) {
////                super.onPostExecute(aLong);
////                if(aLong>0)
////                {
////                    Toast.makeText(getApplicationContext(), "Insert Successfully", Toast.LENGTH_SHORT).show();
////                    setResult(Activity.RESULT_OK);
////                    finish();
////                }
////                else
////                {
////                    Toast.makeText(getApplicationContext(), "Insert Failed", Toast.LENGTH_SHORT).show();
////                }
////            }
////        }.execute();
////    }
////    public void updateItemName(String itemName,String itemPrice)
////    {
////        new AsyncTask<Void, Void, Boolean>() {
////            @Override
////            protected Boolean doInBackground(Void... voids) {
////
////
////                try {
////                    //itemTable = new ItemTable(itemName,Long.parseLong(itemPrice));
////                    itemTable.setItem_name(itemName);
////                    itemTable.setItem_price(Double.parseDouble(itemPrice));
////                }catch (NumberFormatException ex)
////                {
////                    itemTable.setItem_name(itemName);
////                    itemTable.setItem_price(0);
////                    //itemTable = new ItemTable(itemName,0);
////                }
////
////                int update= restaurantDatabase.getItemTableDao().updateItemTableData(itemTable);
////                if(update>0)
////                {
////                    return true;
////                }
////                else
////                {
////                    return false;
////                }
////            }
////
////            @Override
////            protected void onPostExecute(Boolean aLong) {
////                super.onPostExecute(aLong);
////                if(aLong)
////                {
////                    Toast.makeText(getApplicationContext(), "Update Successfully", Toast.LENGTH_SHORT).show();
////                    setResult(Activity.RESULT_OK);
////                    finish();
////                }
////                else
////                {
////                    Toast.makeText(getApplicationContext(), "Update Failed", Toast.LENGTH_SHORT).show();
////                }
////            }
////        }.execute();
////    }
//
//
//}
