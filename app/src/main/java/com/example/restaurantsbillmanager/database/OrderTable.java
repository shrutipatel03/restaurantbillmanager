package com.example.restaurantsbillmanager.database;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class OrderTable implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    int order_id;
    String order_customer_name;
    String order_customer_mobile;
    String order_table_number;
    String order_date;
    double order_total_amount;
    String status;


    public OrderTable() {
    }

    public OrderTable(String order_customer_name, String order_customer_mobile, String order_table_number, String order_date, double order_total_amount, String status) {
        this.order_customer_name = order_customer_name;
        this.order_customer_mobile = order_customer_mobile;
        this.order_table_number = order_table_number;
        this.order_date = order_date;
        this.order_total_amount = order_total_amount;
        this.status = status;
    }

    protected OrderTable(Parcel in) {
        order_id = in.readInt();
        order_customer_name = in.readString();
        order_customer_mobile = in.readString();
        order_table_number = in.readString();
        order_date = in.readString();
        order_total_amount = in.readDouble();
        status = in.readString();
    }

    public static final Creator<OrderTable> CREATOR = new Creator<OrderTable>() {
        @Override
        public OrderTable createFromParcel(Parcel in) {
            return new OrderTable(in);
        }

        @Override
        public OrderTable[] newArray(int size) {
            return new OrderTable[size];
        }
    };

    public double getOrder_total_amount() {
        return order_total_amount;
    }

    public void setOrder_total_amount(double order_total_amount) {
        this.order_total_amount = order_total_amount;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public String getOrder_customer_name() {
        return order_customer_name;
    }

    public void setOrder_customer_name(String order_customer_name) {
        this.order_customer_name = order_customer_name;
    }

    public String getOrder_customer_mobile() {
        return order_customer_mobile;
    }

    public void setOrder_customer_mobile(String order_customer_mobile) {
        this.order_customer_mobile = order_customer_mobile;
    }

    public String getOrder_table_number() {
        return order_table_number;
    }

    public void setOrder_table_number(String order_table_number) {
        this.order_table_number = order_table_number;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(order_id);
        dest.writeString(order_customer_name);
        dest.writeString(order_customer_mobile);
        dest.writeString(order_table_number);
        dest.writeString(order_date);
        dest.writeDouble(order_total_amount);
        dest.writeString(status);
    }
}
