package com.example.restaurantsbillmanager.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {RegisterTable.class,TableTable.class,ItemTable.class,OrderTable.class,OrderItemTable.class}, version = 9,exportSchema = false)
public abstract class RestaurantDatabase extends RoomDatabase {
    public static final String DATABASE_NAME ="restorant_db";
    public abstract RegisterDao getRegisterDao();
    public abstract TableTableDao getTableDao();
    public abstract ItemTableDao getItemTableDao() ;
    public abstract OrderTableDao getOrderTableDao() ;
    public abstract OrderItemDao getOrderItemTableDao();

//    public static RestaurantDatabase getRestaurantDatabase(Context context){
//        return Room.databaseBuilder(context.getApplicationContext(), RestaurantDatabase.class,"restaurant_db").fallbackToDestructiveMigration().build();
//    }

}