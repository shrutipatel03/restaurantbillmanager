package com.example.restaurantsbillmanager.database;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPreference {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String name = "bill_manager";

    public MySharedPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void setUserData(String rastaurant_name, String email, String restourant_owner_name,  String password,String mobile) {
        editor.putBoolean("is_user_login", true);
        editor.putString("restaurant_name", rastaurant_name);
        editor.putString("email", email);
        editor.putString("restourant_owner_name", restourant_owner_name);
        editor.putString("password", password);
        editor.putString("mobile",mobile);
        editor.commit();
    }

    public void logout()
    {
        editor.clear();
        editor.commit();
    }

    public boolean getUserLogin()
    {
        return sharedPreferences.getBoolean("is_user_login", false);
    }
    public String getRestourantOwerName() {return sharedPreferences.getString("restourant_owner_name", ""); }
    public String getRestourantName() {
        return sharedPreferences.getString("restaurant_name", "");
    }
    public String getEmail() {
        return sharedPreferences.getString("email", "");
    }
    public String getUserMobile(){return sharedPreferences.getString("mobile","");}
    public String getPassword(){return sharedPreferences.getString("password","");}


}
