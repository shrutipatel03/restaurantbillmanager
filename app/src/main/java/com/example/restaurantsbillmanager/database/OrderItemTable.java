package com.example.restaurantsbillmanager.database;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class OrderItemTable implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    int order_item_id;
    String order_item_name;
    double order_item_qty;
    double order_item_price;
    double order_total_amount;
    int item_id;
    int order_id;

    public OrderItemTable(String order_item_name, double order_item_qty, double order_item_price, double order_total_amount, int item_id, int order_id) {
        this.order_item_name = order_item_name;
        this.order_item_qty = order_item_qty;
        this.order_item_price = order_item_price;
        this.order_total_amount = order_total_amount;
        this.item_id = item_id;
        this.order_id = order_id;
    }

    protected OrderItemTable(Parcel in) {
        order_item_id = in.readInt();
        order_item_name = in.readString();
        order_item_qty = in.readDouble();
        order_item_price = in.readDouble();
        order_total_amount = in.readDouble();
        item_id = in.readInt();
        order_id = in.readInt();
    }

    public static final Creator<OrderItemTable> CREATOR = new Creator<OrderItemTable>() {
        @Override
        public OrderItemTable createFromParcel(Parcel in) {
            return new OrderItemTable(in);
        }

        @Override
        public OrderItemTable[] newArray(int size) {
            return new OrderItemTable[size];
        }
    };

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getOrder_item_id() {
        return order_item_id;
    }

    public void setOrder_item_id(int order_item_id) {
        this.order_item_id = order_item_id;
    }

    public String getOrder_item_name() {
        return order_item_name;
    }

    public void setOrder_item_name(String order_item_name) {
        this.order_item_name = order_item_name;
    }

    public double getOrder_item_qty() {
        return order_item_qty;
    }

    public void setOrder_item_qty(double order_item_qty) {
        this.order_item_qty = order_item_qty;
    }

    public double getOrder_item_price() {
        return order_item_price;
    }

    public void setOrder_item_price(double order_item_price) {
        this.order_item_price = order_item_price;
    }

    public double getOrder_total_amount() {
        return order_total_amount;
    }

    public void setOrder_total_amount(double order_total_amount) {
        this.order_total_amount = order_total_amount;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(order_item_id);
        dest.writeString(order_item_name);
        dest.writeDouble(order_item_qty);
        dest.writeDouble(order_item_price);
        dest.writeDouble(order_total_amount);
        dest.writeInt(item_id);
        dest.writeInt(order_id);
    }
}
