package com.example.restaurantsbillmanager.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface TableTableDao {
    @Insert
    Long insertTableTable(TableTable tableTable);

    @Query("SELECT * FROM TableTable")
    List<TableTable> fetchAllTableData();

    @Update
    int updateTableData(TableTable tableTable);

    @Delete
    int deleteTableData(TableTable tableTable);
}
