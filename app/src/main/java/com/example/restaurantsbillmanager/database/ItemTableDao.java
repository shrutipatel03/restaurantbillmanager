package com.example.restaurantsbillmanager.database;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface ItemTableDao {

    @Insert
    Long insertItemTable(ItemTable itemTable);

    @Query("SELECT * FROM ItemTable")
    List<ItemTable> fetchAllItemTableData();

    @Update
    int updateItemTableData(ItemTable itemTable);

    @Delete
    int deleteItemTableData(ItemTable itemTable);

}
