package com.example.restaurantsbillmanager.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface RegisterDao {

    @Insert
    Long insertRegisterData(RegisterTable registerTable);

    @Query("select * from registertable where email=:email and password=:password")
    RegisterTable userValidation(String email,String password);

    @Query("select * from registertable where   mobile=:mobile or email=:email")
    RegisterTable userExist(String mobile,String email);
}
