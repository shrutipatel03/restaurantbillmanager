package com.example.restaurantsbillmanager.database;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface OrderTableDao {

    @Insert
    long insertOrderTable(OrderTable orderTable);

    @Query("SELECT * FROM OrderTable")
    List<OrderTable> fetchAllOrderTableData();

    @Query("SELECT * FROM OrderTable where status=:status and DATE(order_date) >=:fromDate and DATE(order_date) <=:toDate")
    List<OrderTable> fetchtoDateOrFromDateOrderTableData(String status,String fromDate,String toDate);

    @Query("SELECT * FROM OrderTable where status=:status and order_table_number=:tableName and DATE(order_date) =:Date")
    List<OrderTable> fetchTableAndDatewiseOrderTableData(String status,String tableName,String Date);

    @Query("SELECT * FROM OrderTable where status=:status and  DATE(order_date) =:Date")
    List<OrderTable> fetchTodayData(String status,String Date);

    @Query("SELECT * FROM OrderTable where status=:status")
    List<OrderTable> getStatus(String status);

    @Update
    int updateOrderTableData(OrderTable orderTable);

    @Delete
    int deleteOrderTableData(OrderTable orderTable);

    @Query("Delete from ordertable where order_id==:order_id")
    int deleteOrderByOrderId(int order_id);

}
