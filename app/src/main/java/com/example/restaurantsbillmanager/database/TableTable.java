package com.example.restaurantsbillmanager.database;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class TableTable implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private int table_id;
    private String table_name;


    public TableTable( String table_name) {
        this.table_name = table_name;
    }

    protected TableTable(Parcel in) {
        table_id = in.readInt();
        table_name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(table_id);
        dest.writeString(table_name);
    }
    @NonNull
    @Override
    public String toString() {
        return table_name;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TableTable> CREATOR = new Creator<TableTable>() {
        @Override
        public TableTable createFromParcel(Parcel in) {
            return new TableTable(in);
        }

        @Override
        public TableTable[] newArray(int size) {
            return new TableTable[size];
        }
    };

    public int getTable_id() {
        return table_id;
    }

    public void setTable_id(int table_id) {
        this.table_id = table_id;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }
}
