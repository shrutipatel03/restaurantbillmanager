package com.example.restaurantsbillmanager.database;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ItemTable implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private int item_id;
    private String item_name;
    private double item_price;

    public ItemTable() {
    }

    public ItemTable(String item_name, double item_price)
    {
        this.item_name=item_name;
        this.item_price=item_price;
    }

    protected ItemTable(Parcel in) {
        item_id = in.readInt();
        item_name = in.readString();
        item_price = in.readDouble();
    }

    public static final Creator<ItemTable> CREATOR = new Creator<ItemTable>() {
        @Override
        public ItemTable createFromParcel(Parcel in) {
            return new ItemTable(in);
        }

        @Override
        public ItemTable[] newArray(int size) {
            return new ItemTable[size];
        }
    };

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public double getItem_price() {
        return item_price;
    }

    public void setItem_price(double item_price) {
        this.item_price = item_price;
    }

    @NonNull
    @Override
    public String toString() {
        return item_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(item_id);
        dest.writeString(item_name);
        dest.writeDouble(item_price);
    }
}
