package com.example.restaurantsbillmanager.database;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface OrderItemDao {

    @Insert
    long insertOrderItem(OrderItemTable orderItemTable);

    @Insert
    long[] bulkInsertOrderItem(OrderItemTable... orderItemTable);

    @Query("SELECT * FROM OrderItemTable")
    List<OrderItemTable> fetchAllOrderItemData();

    @Query("DELETE FROM OrderItemTable WHERE order_id=:order_id")
    int deleteOrderItemByOrderId(int order_id);

    @Query("SELECT * FROM OrderItemTable where order_id==:order_id")
    List<OrderItemTable> fetchOrderIdWiseSubOrderData(int order_id);

    @Update
    int updateOrderItemTableData(OrderItemTable orderItemTable);

    @Delete
    int deleteOrderItemTableData(OrderItemTable orderItemTable);
}
